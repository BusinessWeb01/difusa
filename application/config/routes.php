<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "pages";
//$route['404_override'] = 'pages/error_404';

$route['contacto'] = "pages/contact";
$route['carrito-de-compra'] = "pages/cart";
$route['busqueda'] = "search-products/search";
$route['(^[a-z]+([a-z]|[0-9]|-)*$)'] = "pages/select/$1";
$route['ingredientes'] = "pages/ingredients";
$route['insumo'] = "insumos_controller/paginas";
$route['login'] = "login_controller";
$route['logout'] = "logout_controller";
$route['register'] = "registra_controller";
$route['user-info'] = "panel_controller/hola";
$route['gral-info'] = "panel_controller/info";
$route['validate'] = "login_controller/loguea";
$route['comments'] = "panel_controller/comments";
$route['orders'] = "panel_controller/orders";
$route['suscription'] = "panel_controller/news";
$route['wishes'] = "panel_controller/wishes";
$route['refund'] = "panel_controller/refund";
$route['order-details'] = "panel_controller/details/$1";
$route['wishes-ins-list'] = "articulos_insumos_controller/insertWish";
$route['wishes-com-list'] = "articulos_complementos_controller/insertWish";
$route['productos-insumos'] = "articulos_insumos_controller/productos";
$route['Ins'] = "articulos_insumos_controller/productos";
$route['Inss'] = "insumos_controller/productos";
$route['search'] = "articulos_insumos_controller/filtro";
$route['complementos-ini'] = "articulos_complementos_controller/productos";
$route['complementos-in'] = "complementos_controller/productos";
$route['info-ins'] = "informacion_insumos/producto/$1";
$route['info-com'] = "informacion_complementos/producto/$1";
$route['top_sales'] = "top_sales_controller/getTopSales";
$route['new-news'] = "panel_controller/updateNews";
$route['contacting'] = "contacto_controller/send/Contacto";
$route['team-contacting'] = "contacto_controller/send/Equipo";
$route['admin-info'] = "admin_controller/allUsers";
/* Combinacion product_type */
/*$route['search-products/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/products/$1";
/* Combinacion product_type->page */
/*$route['search-products/([a-z]+([a-z]|[0-9]|-)*)/(:num)'] = "pages/products_type/$1/$3";

/* Combinacion product_type->category */
/*$route['search-products/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/products/$1/$3";
/* Combinacion product_type->category->page */
/*$route['search-products/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)/(:num)'] = "pages/products_category/$1/$3/$5";

/* Combinacion product_type->category->sub_category */
/*$route['search-products/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/products/$1/$3/$5";
/* Combinacion product_type->category->sub->category->page */
/*$route['search-products/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)/(:num)'] = "pages/products_subcategory/$1/$3/$5/$7";*/

$route['product-detail/([a-zA-Z]+([a-zA-Z]|[0-9]|-)*)/([a-z]+([a-z]|[0-9]|-)*)'] = "pages/detail/$1/$3";
$route['agregar-producto'] = "functions/add_to_cart";
$route['remover-del-carrito/(:any)'] = "functions/delete_from_cart/$1";
$route['resumen-de-la-compra'] = "pages/summary_cart";
$route['gracias-por-su-compra'] = "pages/thanks_payment";

//$route['administrador'] = "admin/index";

$route['test'] = "test";
/* End of file routes.php */
/* Location: ./application/config/routes.php */