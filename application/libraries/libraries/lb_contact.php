<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lb_contact{
    
    public function contact_fields(){
        //1: campo requerido
        $send = array(
            'name' => 1,
			'phone' => 1,
            'email' => 1,
			'asunto' => 1,                        
            'comment' => 1
        );
        return $send;
    }
    
    public function validate_fields($data){
        $required_fields = true;
        foreach($this->contact_fields() as $key => $value){
            if($value == 1){
                if(!$data[$key]){
                    $required_fields = false;
                }
            }
        }
        return $required_fields;
    }
    
    public function get_post($data){
        $CI = &get_instance();
	foreach($data as $key => $value){
            $send[$key] = $CI->input->post($key);
	}
	return $send;
    }
    
    public function save(){
        $data = $this->get_post($this->contact_fields());
        if($this->validate_fields($data)){
            $CI = &get_instance();
            $CI->load->model('md_contact');
            date_default_timezone_set('Mexico/General');
            $data['created_at'] = date("Y-m-d H:i:s");
            $status = $CI->md_contact->save($data);
            return $status;
        }
        else{
            return false;
        }
    }
    
    public function get_variables($post){
        $CI = &get_instance();
        $CI->load->library('carousel');
        $send = array(
            'carousel_config' => $CI->carousel->contact(),
			'section_nav' => 'contacto'
		);
        if($post){
            $status = $this->save();
            if($status){
                $send = false;
            }
            else{
                $send['error'] = true;
            }
        }
        return $send;
    }
}