<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Carousel{
    
    public function home(){
		$CI = &get_instance();
        $CI->load->model('md_pages');
        $banners = $this->create_array_for_banners($CI->md_pages->get_banners_for_section());
        return $banners;
    }
    
    function create_array_for_banners($banners, $id_carousel = 'home'){
        $send = false;
        $d = false;
        $aux = false;
        if($banners){
            foreach($banners as $value){
                $aux['path'] = get_option('images_dir').'banners/'.$value->image;
                if($value->url){
                    $aux['link'] = $value->url;
                }
                $d[] = $aux;
            }
            $send = array(
                'carousel_id' => 'carousel_for_'.$id_carousel,
                'carousel_images' => $d
            );
        }
        return $send;
    }
    
    public function contact(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $banners = $this->create_array_for_banners($CI->md_pages->get_banners_for_section('Contacto'), 'contacto');
        return $banners;
    }
    
    public function thanks(){
        $carousel_config = array(
            'carousel_id' => 'carousel-thanks-1-top',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/slide_05.png'
                )
            )
        );
        return $carousel_config;
    }
    
    public function aboutus(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $banners = $this->create_array_for_banners($CI->md_pages->get_banners_for_section('Nosotros'), 'nosotros');
        return $banners;
    }
    
    public function own_ring(){
        $carousel_config = array(
            'carousel_id' => 'carousel-own-ring',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/slide_01.png'
                )
            )
        );
        return $carousel_config;
    }
    
    public function categories(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $banners = $this->create_array_for_banners($CI->md_pages->get_banners_for_section('Anillos de Compromiso'), 'anillos');
        return $banners;
    }
    
    public function ring(){
        $carousel_config = array(
            'carousel_id' => 'carousel-ring',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/slide_01.png'
                )
            )
        );
        return $carousel_config;
    }
    
    public function cart(){
        $carousel_config = array(
            'carousel_id' => 'carousel-cart',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/carrito.jpg'
                )
            )
        );
        return $carousel_config;
    }
    
    public function summary_cart(){
        $carousel_config = array(
            'carousel_id' => 'carousel-summary-cart',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/slide_08.png'
                )
            )
        );
        return $carousel_config;
    }
    
    public function know_your_measure(){
        $carousel_config = array(
            'carousel_id' => 'carousel-summary-cart',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/slide-seccion-01.png'
                )
            )
        );
        return $carousel_config;
    }
    
    public function quality(){
        $carousel_config = array(
            'carousel_id' => 'carousel-summary-cart',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/slide-seccion-02.png'
                )
            )
        );
        return $carousel_config;
    }
    
    public function maintenance(){
        $carousel_config = array(
            'carousel_id' => 'carousel-summary-cart',
            'carousel_images' => array(
                array(
                    'path' => get_option('path_template').'img/slide-seccion-03.png'
                )
            )
        );
        return $carousel_config;
    }
    
    public function collections(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $banners = $this->create_array_for_banners($CI->md_pages->get_banners_for_section('Colecciones'), 'colecciones');
        return $banners;
    }
    
	public function ingredients(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $banners = $this->create_array_for_banners($CI->md_pages->get_banners_for_section('Ingredientes'), 'ingredientes');
        return $banners;
    }
}