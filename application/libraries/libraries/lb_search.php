<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lb_search{
    
    function get_content(){
        $CI = &get_instance();
        $search_text = $CI->input->post('s');
		$send = false;
        if($search_text){
            $search_text = strip_tags($search_text);
            $text = $this->valid_text_of_search($search_text);
			
            if($text){
                //texto valido
                $CI->load->model('md_pages');
				
				$object_type_product = $CI->md_pages->get_product_type_by('slug', $text);
				$object_category = $CI->md_pages->get_category_by('slug', $text);
				$object_sub_category = $CI->md_pages->get_sub_category_by('slug', $text);
				if($object_type_product){
					$send = $this->get_variables($object_type_product,$object_category,$object_sub_category, 1);
				}
            }
        }
        return $send;
    }
	
	public function get_variables($product_type,$category,$sub_category,$page){
		$CI = &get_instance();
		$CI->load->library('carousel');
		$CI->load->model('md_pages');
		
		$complemento_url = '';
		
		$number_of_products = $CI->md_pages->get_number_of_products_searched($product_type,$category,$sub_category);	
		
		$begin_in = $this->get_the_page($page, $number_of_products);
		
		if($product_type && $product_type->slug != ''){
			$complemento_url = $complemento_url.$product_type->slug;
		}
		if($category && $category->slug != ''){
			$complemento_url = $complemento_url.'/'.$category->slug;
		}
		if($sub_category && $sub_category->slug != ''){
			$complemento_url = $complemento_url.'/'.$sub_category->slug;
		}
		echo $number_of_products;
		if($begin_in['status']){
			$send = array(
			'page' => 'products',
			'section_nav' => 'search-products/'.$complemento_url,
			'carousel_config' => $CI->carousel->ingredients(),
			'text_category' => 'Productos',
			'text_subcategory' => 'text_subcategory',
			'products' => $CI->md_pages->get_products($product_type,$category,$sub_category,$begin_in['number']),
			'number_of_products' => $number_of_products,
			'pagination' => $this->get_pagination($page,$number_of_products,$product_type,$category,$sub_category),
			'pagination_numbers' => $this->get_pagination_numbers($page,$number_of_products,$product_type,$category,$sub_category),
			'link_to_pagination' => base_url().'search-products/'.$complemento_url
			);
		}
		else{
			$send = array(
			'page' => 'error_404'
			);
		}
		return $send;
	}
	
	function get_the_page($page, $number_of_items){
		$page = intval($page);
		$begin_in = 0;
		if($page <= 0){
			$page = 1;
		}
		$status = false;
		$number_of_sections = floor($number_of_items/8) + 1;
		if($page <= $number_of_sections){
			$begin_in = ($page - 1) * 8;
			$status = true;
		}
		$send = array('status' => $status, 'number' => $begin_in);
		return $send;
    }
	
	function get_pagination($page,$number_of_items,$product_type,$category,$sub_category){
		$page = intval($page);
		$complemento_url = '';
		if($page <= 0){
			$page = 1;
		}
		$sections = floor($number_of_items/8) + 1;
		$previous = $page - 1;
		$next = $page + 1;
		if($previous <= 0){
			$previous = $sections;
		}
		if($next > $sections){
			$next = 1;
		}
		
		if($product_type && $product_type->slug != ''){
			$complemento_url = $complemento_url.$product_type->slug;
		}
		if($category && $category->slug != ''){
			$complemento_url = $complemento_url.'/'.$category->slug;
		}
		if($sub_category && $sub_category->slug != ''){
			$complemento_url = $complemento_url.'/'.$sub_category->slug;
		}
		
		if($previous == 1){
			$link_previous = base_url().'search-products/'.$complemento_url;
		}
		else{
			$link_previous = base_url().'search-products/'.$complemento_url.'/'.$previous;
		}
		if($next == 1){
			$link_next = base_url().'search-products/'.$complemento_url;
		}
		else{
			$link_next = base_url().'search-products/'.$complemento_url.'/'.$next;
		}
		$send = array(
			'previous' => $link_previous,
			'next' => $link_next
		);
		return $send;
    }
	
	function get_pagination_numbers($page,$number_of_items,$product_type,$category,$sub_category){
		$page = intval($page);
		if($page <= 0){
			$page = 1;
		}
		$sections = floor($number_of_items/8) + 1;
		$send = array();
		for($i = 1; $i <= $sections; $i++){
			$send[$i] = 0;
			if($i == $page){
				$send[$i] = 1;
			}
		}
		return $send;
    }
    
    function valid_text_of_search($text = ''){
        $send = false;
        if($text != ''){
            if (preg_match('/^[a-zA-Z0-9 _-]*$/', $text)) {
                $send = true;
            }
        }
        return $send;
    }
    public function input_values(){
        $data = array(
            'text_search',
            'option_search',
            'option_search_product'
        );
        return $data;
    }
    
    public function valid_values($data = false){
        if($data){
            $send = true;
            foreach($data as $key => $value){
                if(!$value){
                    $send = false;
                }
            }
            return $send;
        }
        else{
            return false;
        }
    }
    
    public function get_search_manu($datos){
        $CI = &get_instance();
        $CI->load->model('md_search');
        $products = $CI->md_search->get_products_for_manu($datos['text_search']);
        return false;
    }
    
    public function get_search_cate($datos){
        echo 'cate';
        return false;
    }
    
    public function get_search_prod($datos){
        echo 'prod';
        return false;
    }
    
    public function get_search($datos = false){
        $send = array(
            's' => '',
	    'products' => false
	);
        if($datos){
            $option_search = 'get_search_'.$datos['option_search'];
            $send['products'] = $this->$option_search($datos);
        }
        return $send;
    }
}