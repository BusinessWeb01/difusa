<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
20150220
Oscar Ju�rez Arriola
Es la clase para el control del carrito de compra
*/
class Lb_cart{
    
    function get_products_of_the_cart(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        foreach($CI->cart->contents() as $value){
            $send[] = $value;
        }
        return $send;
        
    }
    
    function get_amounts(){
        $CI = &get_instance();
        $CI->load->model('md_pages');
        $iva = $CI->md_pages->get_option('iva');
        $iva = (float)$iva;
        $subtotal = 0;
        foreach($CI->cart->contents() as $value){
            $subtotal = $subtotal + $value['subtotal'];
        }
        $iva = ($subtotal / 100) * $iva;
        $send = array(
            'iva' => $iva,
            'subtotal' => $subtotal,
            'total' => $iva + $subtotal
        );
        return $send;
        
    }
    
    function check_payment_data(){
		$CI = &get_instance();
		$send = false;
		if($CI->session->userdata('payment_data')){
			$send = json_decode($CI->session->userdata('payment_data'), true);
		}
		return $send;
    }
    
    function get_content(){
        $CI = &get_instance();
        $CI->load->library('carousel');
        $send = array(
            'carousel_config' => $CI->carousel->cart(),
            'title' => 'Lista de Productos'
        );
        if($CI->cart->total_items() > 0){
            $send['products_of_the_cart'] = $this->get_products_of_the_cart();
            $send['amounts'] = $this->get_amounts();
			$send['payment_data'] = $this->check_payment_data();
        }
		return $send;
    }
}