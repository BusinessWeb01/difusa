<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lb_carro{             //libreria   carrito
   

   
   function agregar($id,$cantidad,$precio,$nombre,$codigo){

      $CI = &get_instance();
      $CI->load->library('cart');
      $carrito = $CI->cart->contents();
 
        foreach ($carrito as $item) {
            //si el id del producto es igual que uno que ya tengamos
            //en la cesta le sumamos uno a la cantidad
            if ($item['id'] == $id) {
                $cantidad = $cantidad + $item['qty'];
            }
        }
        //cogemos los productos en un array para insertarlos en el carrito
        $insert = array(
            'id' => $id,
            'qty' => $cantidad,
            'price' => $precio,
            'name' => $nombre
        );
        //si hay opciones creamos un array con las opciones y lo metemos
        //en el carrito
        if ($codigo) {
            $insert['options'] = array(
            'code'=> $codigo
            );
        }
        //insertamos al carrito
        $CI->cart->insert($insert);

   }
   function eliminarProd($rowid)
   {
    $CI = &get_instance();
    $CI->load->library('cart');
    $producto = array(
            'rowid' => $rowid,
            'qty' => 0
        );
        //después simplemente utilizamos la función update de la librería cart
        //para actualizar el carrito pasando el array a actualizar
        $CI->cart->update($producto);
        
        $CI->session->set_flashdata('productoEliminado', 'El producto fue eliminado correctamente');
   }
   function eliminarCarro()
   {
    $CI = &get_instance();
    $CI->load->library('cart');
    $CI->cart->destroy();
    $CI->session->set_flashdata('destruido', 'El carrito fue eliminado correctamente');
    
   }
}