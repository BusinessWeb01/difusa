<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lb_functions{
    
    public function fields_for_add_product(){
        $send = array(
            'product_name',
            'product_code',
            'product_type',
            'category',
            'sub_category',
            'slug',
			'price',
			'quantity',
			'next_option'
        );
        return $send;
    }
    
    public function get_post($data){
        $CI = &get_instance();
		foreach($data as $key => $value){
			$send[$value] = $CI->input->post($value);
		}
		return $send;
    }
    
    public function exists_in_cart($product_id = 0){
        $CI = &get_instance();
        $item = false;
        foreach ($CI->cart->contents() as $items){
            $items = (object)$items;
			if($items->id == $product_id){
                $item['rowid'] = $items->rowid;
                $item['qty'] = $items->qty;
				$item = (object)$item;
			}
		}
		return $item;
    }
    
    function get_options_for_cart($data, $product){
		$CI = &get_instance();
        $CI->load->model('md_pages');
		$img = $CI->md_pages->get_images_by_table_and_id($product->id_product);

        //ndp 20150728 - modificacion para los puntos por default
		//$puntosD = $CI->md_pages->get_attribute_rings($data['point'], 'point', 'id_rings_point', 'rings_points');
		//if(!$puntosD)
			//$puntosD = $data['point'];

			$send = array(
				//'id_measure' => $data['measure'],
				//'id_color' => $data['color'],
				//'id_point' => $data['point'],
				//'id_kilate' => $data['kilate'],
				//'color' => $CI->md_pages->get_attribute_rings($data['color']),
				//'kilate' => $CI->md_pages->get_attribute_rings($data['kilate'], 'kilate', 'id_rings_kilate', 'rings_kilates'),
				//'point' => $CI->md_pages->get_attribute_rings($data['point'], 'point', 'id_rings_point', 'rings_points'), //ndp 20150728 - Original

				//'point' => $puntosD,

				//'measure' => $CI->md_pages->get_attribute_rings($data['measure'], 'measure', 'id_rings_measure', 'rings_measures'),
				'path_img' => $CI->md_pages->get_option('images_dir').'products/',
				'img' => $img[0],
				//'model' => $CI->md_pages->get_value_table($product->id_product),
				//'model_payment' => $CI->md_pages->get_models_for_kilate_and_point($data['kilate'], $data['point']),
							//'generic_model' => $CI->md_pages->get_generic_model_for_kilate_and_point($data['kilate'], $data['point']),//ndp 20150728 - funcion para traer el modelo generico
				'link_return' => $data['link_return'].'/'.$product->slug
				//'grabado' => $data['grabado']
			);

                //log_message('debug','get_options_for_cart($data, $product).$send = '.print_r($send,TRUE));


		return $send;
    }
    
    public function add_product($referrer){
		$CI = &get_instance();
        $postComplete = $CI->input->post();

        $data = $this->get_post($this->fields_for_add_product());
		$CI = &get_instance();
        $CI->load->model('md_pages');
		
		$product = $CI->md_pages->get_product_by('product_code', $data['product_code']);
		//pre($product);
        if($product){
	
            //ndp 20150728 - modificar la validacion para aceptar 2 anillos del mismo modelo pero con diferentes caracteristicas...
            $item = $this->exists_in_cart($product[0]->id_product);
            if($item){
				//falta poder modiicar el precio
                $product_to_add = array(
                    'rowid' => $item->rowid,
					'qty'   => $data['quantity']
				);
				$CI->cart->update($product_to_add); 
			}
			else {
				$product_to_add = array(
                    'id' => $product[0]->id_product,
					'qty' => $data['quantity'],
					'price' => $data['price'],
					'name' => $product[0]->product_name,
					'code' => $product[0]->product_code,
					'options' => $this->get_options_for_cart($data, $product[0])
				);
				echo 'aqui';
				//pre($product_to_add);
				$CI->cart->product_name_rules = '^.';  
				$CI->cart->insert($product_to_add);
			}
			
			//ndp 20150807 - existe este script =  $('#next_option').val('list');
            if($data['next_option'] == 'list'){
                //return
                $referrer = '/carrito-de-compra';
            }
            else{
                $referrer = $data['link_return'];
            }
            return $referrer;
        }
        else{
            return '';
        }
    }
    
    function is_rowid($rowid){
	$CI = &get_instance();
	$send = false;
	if($CI->cart->total_items() > 0){
	    foreach($CI->cart->contents() as $items){
		if($items['rowid'] == $rowid){
		    $send = true;
		}
	    }
	}
	return $send;
    }
    
    function delete_product($referrer, $rowid){
	$redirect = '';
	if($this->is_rowid($rowid)){
	    $CI = &get_instance();
	    $data = array(
		'rowid' => $rowid,
		'qty'   => $CI->cart->CI->session->userdata['cart_contents'][$rowid]['qty'] - 1
	    );
	    $CI->cart->update($data);
	    $redirect = $referrer;
	}
	return $redirect;
    }
	
	function get_the_time($text = ''){
		$y = substr($text, 0, 4);
		
		$m = substr($text, 4, 2);
		
		$d = substr($text, 6, 2);
		
		$h = substr($text, 8, 2);
		
		$min = substr($text, 10, 2);
		
		$seg = substr($text, 12, 2);
		$time = $y.'-'.$m.'-'.$d.' '.$h.':'.$min.':'.$seg;
		return $time;
		
	}
	
	function obtener_codigo_de_estado($estado = ''){
		$lista_estados = array(
			'Aguascalientes' => 'AGS',
			'Baja California' => 'BC',
			'Baja California Sur' => 'BCS',
			'Campeche' => 'CMP',
			'Chiapas' => 'CHS',
			'Chihuahua' => 'CHI',
			'Coahuila' => 'COA',
			'Colima' => 'COL',
			'Distrito Federal' => 'DF',
			'Durango' => 'DGO',
			'Estado de Mexico' => 'MEX',
			'Guanajuato' => 'GTO',
			'Guerrero' => 'GRO',
			'Hidalgo' => 'HGO',
			'Jalisco' => 'JAL',
			'Michoacán' => 'MCH',
			'Morelos' => 'MOR',
			'Nayarit' => 'NAY',
			'Nuevo León' => 'NL',
			'Oaxaca' => 'OAX',
			'Puebla' => 'PUE',
			'Querétaro' => 'QRO',
			'Quintana Roo' => 'QR',
			'San Luis Potosí' => 'SLP',
			'Sinaloa' => 'SIN',
			'Sonora' => 'SON',
			'Tabasco' => 'TAB',
			'Tamaulipas' => 'TMS',
			'Tlaxcala' => 'TLX',
			'Veracruz' => 'VER',
			'Yucatán' => 'YUC',
			'Zacatecas' => 'ZAC' //ndp 20150715 - agregado
		);

                $send = "UUU";

                
                if (array_key_exists($estado, $lista_estados))
                    $send= $lista_estados[$estado];

                try{
                }catch(Exception $e){

                }
                
                return $send;
	}
	
	function create_file_for_sap($data = false){
		//ndp 20150728 - creacion y modificaciones del archivo
		log_message('debug', 'lb_functions.php.create_file_for_sap().$data = '.print_r($data,TRUE));
	
	
		if($data){
			$info_cart = json_decode($data->informacion, true);
			//pre($data);
			//pre($info_cart);

                        //ndp 20150811 - se quita la linea de facturacion cuando no hayan datos
                        /*
			if(!$info_cart['rfc_factura']){
				$info_cart['rfc_factura'] = 'XXXX010101000';
			}
			*/
			$estado_factura = $this->obtener_codigo_de_estado($info_cart['estado_factura']);
			$estado_envio = $this->obtener_codigo_de_estado($info_cart['estado_envio']);
			
			$e1 = 'E1|'.$data->folio.'|'.$info_cart['nombre_cliente'].' '.$info_cart['apellido_paterno_cliente'].' '.$info_cart['apellido_materno_cliente'].'|'.$info_cart['telefono_cliente'].'|'.$info_cart['celular_cliente'].'|'.$info_cart['email_cliente'].'|||||||||';
			
			$e2 = 'E2|'.$info_cart['nombre_envio'].'|'.$info_cart['calle_envio'].'|'.$info_cart['num_exterior_envio'].'|'.$info_cart['num_interior_envio'].'|'.$info_cart['colonia_envio'].'|'.$info_cart['localidad_envio'].'|'.$info_cart['delegacion_envio'].'|'.$estado_envio.'|'.$info_cart['cp_envio'].'|MX|'.$info_cart['telefono_1_envio'].'|'.$info_cart['telefono_2_envio'].'|'.$info_cart['referencia_envio'].'|'.$info_cart['descripcion_envio'];
                        $e3 = '';
                        if($info_cart['rfc_factura']){
                            $e3 = 'E3|'.$info_cart['razon_social_factura'].'|'.$info_cart['calle_factura'].'|'.$info_cart['num_exterior_factura'].'|'.$info_cart['num_interior_factura'].'|'.$info_cart['colonia_factura'].'|'.$info_cart['localidad_factura'].'|'.$info_cart['delegacion_factura'].'|'.$estado_factura.'|'.$info_cart['cp_factura'].'|MX|'.$info_cart['rfc_factura'].'|'.$info_cart['email_factura'].'||';
                        }
			
			$importe = 0;
			foreach($info_cart['session_user']['cart_contents'] as $value){
				$importe = ($value['price']*$value['qty'])+$importe;
			}
			if($data->metodo_pago == 'paypal'){
				$e4 = 'E4|'.$data->metodo_pago.'|'.$info_cart['email_cliente'].'||'.$importe.'||||||||||';
			}
			else{
				$info_respuesta = json_decode($data->respuesta_metodo_pago, true);
				$bancomer_respuesta = $info_respuesta['post'];
				//pre($bancomer_respuesta);
				if($bancomer_respuesta['plazos'] != 0){
					$e4 = 'E4|multipagos|'.$bancomer_respuesta['financiado'].'|'.$bancomer_respuesta['plazos'].'|'.$importe.'||||||||||';
				}
				else{
					$e4 = 'E4|multipagos|||'.$importe.'||||||||||';
				}
			}
			
			
			$products = array();
			foreach($info_cart['session_user']['cart_contents'] as $value){
				if(isset($value['price'])){
					//ndp 20150715 - modificacion al layout
					$products[] = 'D1|'.$value['code'].'|'.$value['qty'].'|'.$value['price'].'|'.' '.'|'.$value['code'].'|'.$value['code'].'|'.$value['code'].'|'.$value['code'];//|||||||||';
				}
			}
			$content = $e1.'<br>'.$e2.'<br>'.$e3.'<br>'.$e4;
			foreach($products as $value){
				$content = $content.'<br>'.$value;
			}
			
			$time_send = str_replace(':','',str_replace(' ','',str_replace('-','',$data->fecha_registro)));
			$file_name = $time_send.'_'.$data->folio;
			$path = 'content/files/'.$file_name.'.txt';
			
			$e1 = str_replace("|","\t",$e1);
			$e2 = str_replace("|","\t",$e2);
			$e3 = str_replace("|","\t",$e3);
			$e4 = str_replace("|","\t",$e4);
			
			$content_file = $e1."\r\n".$e2."\r\n".$e3."\r\n".$e4."\r\n";
			
			foreach($products as $value){
				$dn = str_replace("|","\t",$value);
				$content_file = $content_file.$dn."\r\n";
			}
			$fp = fopen($path,"w");
			$content_file = mb_convert_encoding($content_file, 'us-ascii', 'utf-8');
			fwrite($fp, $content_file);
			fclose($fp);
			$this->send_file_sap($path, $data, $info_cart);
		}
	}
	//ndp 20150715  funcion agregada
	function getClaveColor($colorLetra){
	
		log_message('debug', '...getClaveColor($colorLetra)=='.$colorLetra);
		$lista_claves = array(
				'Oro Amarillo' => 'OAMA',			
				'Oro Blanco' => 'OBLA',
				'Oro Rosa' => 'OROS',			
				'Oro Combinado' => 'OCOM',
			);
			return $lista_claves[$colorLetra];
	}
	
	function send_file_sap($file = '', $data, $info_cart){
            log_message('debug', 'function send_file_sap(1,2,3).ENVIANDO CORREO...');

	
               $CI = &get_instance();
               $config = Array(
                    'protocol' => 'smtp',
                    //'smtp_host' => 'mail.klamore.com.mx',
                    //'smtp_port' => 587,
                    'smtp_host' => 'ssl://mail.klamore.com.mx',
                    'smtp_port' => 465,
                    'smtp_user' => 'ventas@klamore.com.mx',
                    'smtp_pass' => 'UP3U5bng',
                    'mailtype'  => 'html',
                    'charset'   => 'iso-8859-1'
                );

		//$CI->load->library('email');
                $CI->load->library('email', $config);
		$data_send = array(
			'data' => $data,
			'info_cart' => $info_cart
		);
		//pedidos.bogue@grupobogue.com.mx
		$list = array('pedidos.bogue@grupobogue.com.mx', 'dgalvan@grupobogue.com.mx', 'jaguilar@grupobogue.com.mx', 'finanzasbogue@grupobogue.com.mx', 'procesosbogue@grupobogue.com.mx', 'daniel.planas@bwell-it.com');
		//$list = array('daniel.planas@bwell-it.com');
		//$list = array('daniel.planas@bwell-it.com','disenos4@grupobogue.com.mx', 'procesosbogue@grupobogue.com.mx');
		$config['mailtype'] = 'html'; 
		$CI->email->initialize($config);
		$CI->email->from('ventas@klamore.com.mx', 'klamore.com.mx');
		$CI->email->to($list);
		//$CI->email->bcc('daniel.planas@bweb.mx');
		$CI->email->subject('Compra en tienda klamore.com.mx | Folio '.$data->folio);
		$message = $CI->load->view('klamore/email_sap', $data_send, TRUE);
		$CI->email->message($message);
		$CI->email->attach($file);
		$CI->email->send();

          
	}	
}