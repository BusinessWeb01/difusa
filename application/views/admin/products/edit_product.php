<div class="row-fluid">
    <div class="span12">
        <?php if($product_error == 'required'):  ?>
            <div class="alert alert-error">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <strong>Por favor.</strong> Los campos con (*) son necesario.
            </div>
        <?php endif; ?>
        <?php if($product_success): ?>
            <div class="alert alert-success">
                <button data-dismiss="alert" class="close" type="button">×</button>
                El producto se edito correctamente.
            </div>
        <?php endif; ?>
        <form id = "edit_product_form" class="form-horizontal" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>products/edit" enctype="multipart/form-data">
            <table class="table table-striped">
                <tbody>
		    <tr>
			<td>
			    <label for="name">
				*Nombre
                            </label>
                        </td>
                        <td>
			    <input class="input-xxlarge" type="text" id="name" name="name" <?php if($info_product){ if(isset($info_product[0]->name)){ echo 'value="'.$info_product[0]->name.'"'; } } ?>/>
			</td>
                    </tr>
		    <tr>
			<td>
			    <label for="model">
				*Modelo
                            </label>
                        </td>
                        <td>
			    <input class="input-xxlarge" type="text" id="model" name="model" <?php if($info_product){ if(isset($info_product[0]->model)){ echo 'value="'.$info_product[0]->model.'"'; } } ?>/>
			</td>
                    </tr>
		    <tr>
			<td>
			    <label for="slug">
				*Slug
                            </label>
                        </td>
                        <td>
			    <input class="input-xxlarge" type="text" id="slug" name="slug" <?php if($info_product){ if(isset($info_product[0]->slug)){ echo 'value="'.$info_product[0]->slug.'"'; } } ?>/>
			</td>
                    </tr>
		    <tr>
			<td>
			    <label for="price">
				*Precio
                            </label>
                        </td>
                        <td>
			    <input class="input-xxlarge" type="text" id="price" name="price" <?php if($info_product){ if(isset($info_product[0]->price)){ echo 'value="'.$info_product[0]->price.'"'; } } ?>/>
			</td>
                    </tr>
		    <tr>
			<td>
			    <label for="description">
				Descripción
                            </label>
                        </td>
                        <td>
			    <textarea class="input-xxlarge" id="description" name="description"><?php if($info_product){ if(isset($info_product[0]->description)){ echo $info_product[0]->description; } } ?></textarea>
                        </td>
                    </tr>
		    <?php if($colors): ?>
			<tr>
			    <td>
				<label for="id_rings_color">
				    Color
				</label>
			    </td>
			    <td>
				<select id="id_rings_color" name="id_rings_color" class="input-xxlarge">
				    <option>...</option>
				    <?php foreach($colors as $value): ?>
					<?php if($info_product){
					    if(isset($info_product[0]->id_rings_color)){
						if($info_product[0]->id_rings_color == $value->id_rings_color){
						    echo '<option value="'.$value->id_rings_color.'" selected>'.$value->color.'</option>';
						}
						else{
						    echo '<option value="'.$value->id_rings_color.'">'.$value->color.'</option>';
						}
					    }
					} ?>
				    <?php endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
		    <?php if($kilates): ?>
			<tr>
			    <td>
				<label for="id_rings_kilate">
				    Kilate
				</label>
			    </td>
			    <td>
				<select id="id_rings_kilate" name="id_rings_kilate" class="input-xxlarge">
				    <option>...</option>
				    <?php foreach($kilates as $value): ?>
					<?php if($info_product){
					    if(isset($info_product[0]->id_rings_kilate)){
						if($info_product[0]->id_rings_kilate == $value->id_rings_kilate){
						    echo '<option value="'.$value->id_rings_kilate.'" selected>'.$value->kilate.'</option>';
						}
						else{
						    echo '<option value="'.$value->id_rings_kilate.'">'.$value->kilate.'</option>';
						}
					    }
					} ?>
				    <?php endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
                   <!-- ndp 20150718 - puntos default -->
                    <tr>
			<td>
			    <label for="id_default_points">
				Puntos por Default
                            </label>
                        </td>
                        <td>
			    <input class="input-xxlarge" type="text" id="default_points" name="default_points" <?php if($info_product){ if(isset($info_product[0]->default_points)){ echo 'value="'.$info_product[0]->default_points.'"'; } } ?>/>
			</td>
                    </tr>

		    <?php if($points): ?>
			<tr>
			    <td>
				<label for="id_rings_point">
				    Puntos (precios universales)
				</label>
			    </td>
			    <td>
				<select id="id_rings_point" name="id_rings_point" class="input-xxlarge">
				    <option>...</option>
				    <?php foreach($points as $value): ?>
					<?php if($info_product){
					    if(isset($info_product[0]->id_rings_point)){
						if($info_product[0]->id_rings_point == $value->id_rings_point){
						    echo '<option value="'.$value->id_rings_point.'" selected>'.$value->point.'</option>';
						}
						else{
						    echo '<option value="'.$value->id_rings_point.'">'.$value->point.'</option>';
						}
					    }
					} ?>
				    <?php endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
                    <tr id="imagenes">
                        <td><label for="img_product">Imagen:</label></td>
                        <td>
			    <input type="file" id="img_product" name="img_product[]" multiple />
			    <?php if($info_product):
				if(isset($info_product[0]->img)):
				    if($info_product[0]->img): ?>
				    <div>
					<?php foreach($info_product[0]->img as $value): ?>
					    <a href="<?php echo base_url(); ?>products/delete_img/<?php echo $value->id_product_meta;?>" class="link-remove-img-pro">
						<i class="icon-remove"></i>
					    </a>
					    <img src="<?php echo get_option('path_template'); ?>timthumb/timthumb.php?src=<?php echo get_option('images_dir'); ?>products/<?php echo $value->meta_value; ?>&amp;h=75&amp;w=75&amp;zc=2" alt="imagen" />
					<?php endforeach; ?>
				    </div>
				    <?php endif;
				endif;
			    endif; ?>
			</td>
                    </tr>
		    <tr>
				<td>
					<label for="ring">
						Anillo
					</label>
				</td>
                <td>
					<?php if($info_product){
						if(isset($info_product[0]->ring)){
							if($info_product[0]->ring == 1){
								echo '<input type="checkbox" id="ring" name="ring" value="1" checked/>';
							}
							else{
								echo '<input type="checkbox" id="ring" name="ring" value="1"/>';
							}
						}
					} ?>
				</td>
            </tr>
			
			<tr>
				<td>
					<label for="promotion">
						Promoción
					</label>
				</td>
                <td>
					<?php if($info_product){
						if(isset($info_product[0]->promotion)){
							if($info_product[0]->promotion == 1){
								echo '<input type="checkbox" id="promotion" name="promotion" value="1" checked/>';
							}
							else{
								echo '<input type="checkbox" id="promotion" name="promotion" value="1"/>';
							}
						}
					} ?>
				</td>
            </tr>
			
		    <?php if(isset($categories_rings)): ?>
			<tr>
			    <td>
				<label for="categories_rings">
				    Categoría de Anillo de compromiso:
				</label>
			    </td>
			    <td>
				<select class="input-xxlarge" id="categories_rings" name="categories_rings">
				    <option value="0">Ninguna</option>
				    <?php foreach($categories_rings as $value):
					if($value->id_category_rings != 3): ?>
					    <?php if($info_product){
						if(isset($info_product[0]->category_rings_product[0]->id_category_rings)){
						    if($info_product[0]->category_rings_product[0]->id_category_rings == $value->id_category_rings){
							echo '<option value="'.$value->id_category_rings.'" selected>'.$value->name.'</option>';
						    }
						    else{
							echo '<option value="'.$value->id_category_rings.'">'.$value->name.'</option>';
						    }
						}
						else{
						    echo '<option value="'.$value->id_category_rings.'">'.$value->name.'</option>';
						}
					    }
					    else{
						echo '<option value="'.$value->id_category_rings.'">'.$value->name.'</option>';
					    }?>
					<?php endif;
				    endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
		    <?php if(isset($categories_collections)): ?>
			<tr>
			    <td>
				<label for="category_collection">
				    Colección:
				</label>
			    </td>
			    <td>
				<select id="category_collection" name="category_collection" class="input-xxlarge">
				    <option value="0">Ninguna</option>
				    <?php foreach($categories_collections as $value): //$info_product[0]->category_collection_product[0]->id_rel_category_collection_product; ?>
					<?php if($info_product){
					    if(isset($info_product[0]->category_collection_product[0]->id_category_collection)){
						if($info_product[0]->category_collection_product[0]->id_category_collection == $value->id_category_collection){
						    echo '<option value="'.$value->id_category_collection.'" selected>'.$value->name.'</option>';
						}
						else{
						    echo '<option value="'.$value->id_category_collection.'">'.$value->name.'</option>';
						}
					    }
					    else{
						echo '<option value="'.$value->id_category_collection.'">'.$value->name.'</option>';
					    }
					}
					else{
					    echo '<option value="'.$value->id_category_collection.'">'.$value->name.'</option>';
					}?>
				    <?php endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
                </tbody>
            </table>
            <div class="form-actions">
		<?php if($info_product){
		    if(isset($info_product[0]->id_product)){
			echo '<input type="hidden" name="edit_product" value="'.$info_product[0]->id_product.'">';
		    }
		} ?>
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a class="btn" href="<?php echo base_url(); ?>admin/products">Cancelar</a>
            </div>
        </form>
    </div>
</div>
<script src="<?php echo get_option('path_admin'); ?>js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/messages_es.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/admin_scripts.js" type="text/javascript"></script>