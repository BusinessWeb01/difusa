<div class="row-fluid">
	<div class="span12">
		<?php if($product_error == 'required'):  ?>
			<div class="alert alert-error">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <strong>Por favor.</strong> Los campos con (*) son necesario.
            </div>
        <?php endif; ?>
        <?php if($product_success): ?>
            <div class="alert alert-success">
                <button data-dismiss="alert" class="close" type="button">×</button>
                El producto se guardo correctamente.
            </div>
        <?php endif; ?>
        <form id = "edit_product_form" class="form-horizontal" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>products/add" enctype="multipart/form-data">
			<table class="table table-striped">
				<tbody>
					<tr>
						<td>
							<label for="name">
								*Nombre
							</label>
                        </td>
						<td>
							<input class="input-xxlarge" type="text" id="name" name="name" />
						</td>
                    </tr>
					<tr>
						<td>
							<label for="model">
								*Modelo
                            </label>
                        </td>
                        <td>
							<input class="input-xxlarge" type="text" id="model" name="model" />
						</td>
                    </tr>
					<tr>
						<td>
							<label for="slug">
								*Slug
                            </label>
                        </td>
                        <td>
							<input class="input-xxlarge" type="text" id="slug" name="slug" />
						</td>
                    </tr>
					<tr>
						<td>
							<label for="price">
								*Precio
                                                        </label>
                                                </td>
                                                <td>
							<input class="input-xxlarge" type="text" id="price" name="price" />
						</td>
                    </tr>
					<tr>
						<td>
							<label for="description">
								Descripción
                            </label>
                        </td>
                        <td>
							<textarea class="input-xxlarge" id="description" name="description"></textarea>
                        </td>
                    </tr>
					<?php if($colors): ?>
					<tr>
						<td>
						<label for="id_rings_color">
							Color
						</label>
						</td>
						<td>
						<select id="id_rings_color" name="id_rings_color" class="input-xxlarge">
							<option>...</option>
							<?php foreach($colors as $value): ?>
							<option value="<?php echo $value->id_rings_color; ?>"><?php echo $value->color; ?></option>
							<?php endforeach; ?>
						</select>
						</td>
					</tr>
					<?php endif; ?>
					<?php if($kilates): ?>
					<tr>
						<td>
						<label for="id_rings_kilate">
							Kilate
						</label>
						</td>
						<td>
						<select id="id_rings_kilate" name="id_rings_kilate" class="input-xxlarge">
							<option>...</option>
							<?php foreach($kilates as $value): ?>
							<option value="<?php echo $value->id_rings_kilate; ?>"><?php echo $value->kilate; ?></option>
							<?php endforeach; ?>
						</select>
						</td>
					</tr>
					<?php endif; ?>

                                        <!--ndp 20150717 Puntos default -->
                                        <tr>
						<td>

                                                    <!-- ndp 20150717 Puntos precios Universales -->
						<label for="id_default_points">
							Puntos por Default
						</label>
						</td>
						<td>
                                                    <input class="input-xxlarge" type="text" id="default_points" name="default_points" />
						</td>
					</tr>

					<?php if($points): ?>
					<tr>
						<td>

                                                    <!-- ndp 20150717 Puntos precios Universales -->
						<label for="id_rings_point">
							Puntos (precios universales)
						</label>
						</td>
						<td>
						<select id="id_rings_point" name="id_rings_point" class="input-xxlarge">
							<option>...</option>
							<?php foreach($points as $value): ?>
							<option value="<?php echo $value->id_rings_point; ?>"><?php echo $value->point; ?></option>
							<?php endforeach; ?>
						</select>
						</td>
					</tr>
					<?php endif; ?>
                    <tr>
                        <td><label for="img_product">Imagen:</label></td>
                        <td><input type="file" id="img_product" name="img_product[]" multiple /></td>
                    </tr>
					<tr>
						<td>
							<label for="ring">
								Anillo
                            </label>
                        </td>
                        <td>
							<input type="checkbox" id="ring" name="ring" value="1" />
						</td>
                    </tr>
					<tr>
						<td>
							<label for="promotion">
								Promoción
                            </label>
                        </td>
                        <td>
							<input type="checkbox" id="promotion" name="promotion" value="1" />
						</td>
                    </tr>
		    <?php if(isset($categories_rings)): ?>
			<tr>
			    <td>
				<label for="categories_rings">
				    Categoría de Anillo de compromiso:
				</label>
			    </td>
			    <td>
				<select class="input-xxlarge" id="categories_rings" name="categories_rings">
				    <option value="0">Ninguna</option>
				    <?php foreach($categories_rings as $value):
					if($value->id_category_rings != 3): ?>
					    <option value="<?php echo $value->id_category_rings; ?>"><?php echo $value->name; ?></option>
					<?php endif;
				    endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
		    <?php if(isset($categories_collections)): ?>
			<tr>
			    <td>
				<label for="category_collection">
				    Colección:
				</label>
			    </td>
			    <td>
				<select id="category_collection" name="category_collection" class="input-xxlarge">
				    <option value="0">Ninguna</option>
				    <?php foreach($categories_collections as $value): ?>
					<option value="<?php echo $value->id_category_collection; ?>"><?php echo $value->name; ?></option>
				    <?php endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
                </tbody>
            </table>
            <div class="form-actions">
                <input type="hidden" name="add_product" value="1">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a class="btn" href="<?php echo base_url(); ?>admin/products">Cancelar</a>
            </div>
        </form>
    </div>
</div>
<script src="<?php echo get_option('path_admin'); ?>js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/messages_es.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/admin_scripts.js" type="text/javascript"></script>