<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Administrador</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
		<meta name="author" content="Muhammad Usman">
		<link id="bs-css" href="<?php echo site_url();?>content/themes/admin/css/bootstrap-classic.css" rel="stylesheet">
		<style type="text/css">
			body{
				padding-bottom: 40px;
			}
			.sidebar-nav {
				padding: 9px 0;
			}
			.link-remove-img-pro:hover, .link-remove-img-pro:focus{
				outline: 0;
				text-decoration: none;
			}
		</style>
		<?php if (isset($css_files)): ?>
			<?php foreach($css_files as $file): ?>
				<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
			<?php endforeach; ?>
			<?php foreach($js_files as $file): ?>
				<script src="<?php echo $file; ?>"></script>
			<?php endforeach; ?>
		<?php endif ?>
		<link href="<?php echo site_url();?>content/themes/admin/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="<?php echo site_url();?>content/themes/admin/css/charisma-app.css" rel="stylesheet">
		<link href="<?php echo site_url();?>content/themes/admin/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
		<link href='<?php echo site_url();?>content/themes/admin/css/fullcalendar.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
		<link href='<?php echo site_url();?>content/themes/admin/css/chosen.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/uniform.default.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/colorbox.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/jquery.cleditor.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/jquery.noty.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/noty_theme_default.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/elfinder.min.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/elfinder.theme.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/jquery.iphone.toggle.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/opa-icons.css' rel='stylesheet'>
		<link href='<?php echo site_url();?>content/themes/admin/css/uploadify.css' rel='stylesheet'>
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- The fav icon -->
		<link rel="shortcut icon" href="img/favicon.ico">
	</head>
	<body>
	<!-- topbar starts -->
		<div class="navbar">
			<div class="navbar-inner">
				<div class="container-fluid">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<div class="btn-group pull-left">
						<?php echo anchor('', str_replace('/','',str_replace('http://','',base_url()))); ?>
					</div>	
					<!-- <img alt="Charisma Logo" src="img/logo20.png" /> <span>Charisma</span> -->				
					<!-- user dropdown starts -->
					<div class="btn-group pull-right">
						<?php echo anchor('/auth/logout/', 'Salir'); ?>
					</div>
					<!-- user dropdown ends -->
					<div class="top-nav nav-collapse">
						<ul class="nav">
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>
		</div>
		<!-- topbar ends -->
		<div class="container-fluid">
			<div class="row-fluid">
				<!-- left menu starts -->
				<div class="span2 main-menu-span">
					<div class="well nav-collapse sidebar-nav">
						<ul class="nav nav-tabs nav-stacked main-menu">
							<li class="nav-header hidden-tablet">Opciones</li>
							<li><a href="<?php echo base_url();?>admin/banners"><i><img src="<?php echo base_url(); ?>content/themes/admin/images/productos.png" alt="imagen"></i><span class="hidden-tablet"> Banners</span></a></li>
							<li><a href="<?php echo base_url();?>admin/pages"><i><img src="<?php echo base_url(); ?>content/themes/admin/images/productos.png" alt="imagen"></i><span class="hidden-tablet"> Páginas</span></a></li>
							<li><a href="<?php echo base_url();?>admin/options"><i><img src="<?php echo base_url(); ?>content/themes/admin/images/usuario.png" alt="Imagen"></i><span class="hidden-tablet"> Información</span></a></li>
							<li><a href="<?php echo base_url();?>admin/users"><i><img src="<?php echo base_url(); ?>content/themes/admin/images/usuario.png" alt="Imagen"></i><span class="hidden-tablet"> Usuarios</span></a></li>
						</ul>
					</div><!--/.well -->
				</div><!--/span-->
				<!-- left menu ends -->
				<div id="content" class="span10">
					<!-- content starts -->
					<div class="row-fluid sortable">
						<div class="box span12">
							<div class="box-header well" data-original-title>
								<h2>
									<?php if(isset($variable['title_form'])){
										echo $variable['title_form'];
									}
									if(isset($title_form)){
										echo $title_form;
									} ?>
								</h2>
								<?php if(isset($variable['title_form'])): ?>
									<?php if($variable['title_form'] == 'Usuarios-'): ?>
										<p class="text-right">
											<a class="btn btn-small" href="<?php echo base_url(); ?>export/users"><i class="icon-download-alt"></i> Exportar usuarios</a>
										</p>
									<?php endif; ?>
								<?php endif; ?>
							</div>
							<div class="box-content">
								<?php
									if(isset($variable['activate_add_product'])){
										$this->load->view('admin/products/top_content');
									}
									if(isset($variable['activate_add_collection'])){
										$this->load->view('admin/collections/top_content');
									}
									if(isset($output)){
										echo $output;
									}
									else{
										if(isset($add_product)){
											$this->load->view('admin/products/add_product');
										}
										else{
											if(isset($edit_product)){
												$this->load->view('admin/products/edit_product');
											}
											else{
												if(isset($add_collection)){
													$this->load->view('admin/collections/add_collection');
												}
												else{
													if(isset($actived_view_invo)){
														$this->load->view('admin/view_invo', array('payment' => $payment));
													}
												}
											}
										}
									}
								?>
							</div>
						</div><!--/span-->
					</div><!--/row-->
				</div>
			</div><!--/span-->
		</div><!--/row-->
		<hr>
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		<footer>
		</footer>
		<!-- external javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<!-- jQuery -->
		<!-- transition / effect library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-transition.js"></script>
		<!-- alert enhancer library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-alert.js"></script>
		<!-- modal / dialog library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-modal.js"></script>
		<!-- custom dropdown library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-dropdown.js"></script>
		<!-- scrolspy library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-scrollspy.js"></script>
		<!-- library for creating tabs -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-tab.js"></script>
		<!-- library for advanced tooltip -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-tooltip.js"></script>
		<!-- popover effect library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-popover.js"></script>
		<!-- button enhancer library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-button.js"></script>
		<!-- accordion library (optional, not used in demo) -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-collapse.js"></script>
		<!-- carousel slideshow library (optional, not used in demo) -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-carousel.js"></script>
		<!-- autocomplete library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-typeahead.js"></script>
		<!-- tour library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/bootstrap-tour.js"></script>
		<!-- library for cookie management -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.cookie.js"></script>
		<!-- calander plugin -->
		<script src='<?php echo site_url();?>content/themes/admin/js/fullcalendar.min.js'></script>
		<!-- data table plugin -->
		<script src='<?php echo site_url();?>content/themes/admin/js/jquery.dataTables.min.js'></script>
		<!-- chart libraries start -->
		<script src="<?php echo site_url();?>content/themes/admin/js/excanvas.js"></script>
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.flot.min.js"></script>
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.flot.pie.min.js"></script>
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.flot.stack.js"></script>
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.flot.resize.min.js"></script>
		<!-- chart libraries end -->
		<!-- select or dropdown enhancer -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.chosen.min.js"></script>
		<!-- checkbox, radio, and file input styler -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.uniform.min.js"></script>
		<!-- plugin for gallery image view -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.colorbox.min.js"></script>
		<!-- rich text editor library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.cleditor.min.js"></script>
		<!-- notification plugin -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.noty.js"></script>
		<!-- file manager library -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.elfinder.min.js"></script>
		<!-- star rating plugin -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.raty.min.js"></script>
		<!-- for iOS style toggle switch -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.iphone.toggle.js"></script>
		<!-- autogrowing textarea plugin -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.autogrow-textarea.js"></script>
		<!-- multiple file upload plugin -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.uploadify-3.1.min.js"></script>
		<!-- history.js for cross-browser state change on ajax -->
		<script src="<?php echo site_url();?>content/themes/admin/js/jquery.history.js"></script>
		<!-- application script for Charisma demo -->

	</body>
</html>