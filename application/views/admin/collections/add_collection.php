<div class="row-fluid">
    <div class="span12">
        <?php if($error == 'required'):  ?>
            <div class="alert alert-error">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <strong>Por favor.</strong> Los campos con (*) son necesario.
            </div>
        <?php endif; ?>
        <?php if($success): ?>
            <div class="alert alert-success">
                <button data-dismiss="alert" class="close" type="button">×</button>
                La colección se guardo correctamente.
            </div>
        <?php endif; ?>
        <form id = "edit_product_form" class="form-horizontal" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>collections/add" enctype="multipart/form-data">
            <table class="table table-striped">
                <tbody>
		    <tr>
			<td>
			    <label for="name">
				*Nombre
                            </label>
                        </td>
                        <td>
			    <input class="input-xxlarge" type="text" id="name" name="name" />
			</td>
                    </tr>
		    <tr>
			<td>
			    <label for="slug">
				*Slug
                            </label>
                        </td>
                        <td>
			    <input class="input-xxlarge" type="text" id="slug" name="slug" />
			</td>
                    </tr>
		    <tr>
			<td>
			    <label for="description">
				Descripción
                            </label>
                        </td>
                        <td>
			    <textarea class="input-xxlarge" id="description" name="description"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="img_product">Imagen:</label></td>
                        <td><input type="file" id="img_product" name="img_product[]" multiple /></td>
                    </tr>
		    <?php if($collections): ?>
			<tr>
			    <td>
				<label for="id_collection">
				    *Sección:
				</label>
			    </td>
			    <td>
				<select id="id_collection" name="id_collection" class="input-xxlarge">
				    <option>...</option>
				    <?php foreach($collections as $value): ?>
					<option value="<?php echo $value->id_collection; ?>"><?php echo $value->name; ?></option>
				    <?php endforeach; ?>
				</select>
			    </td>
			</tr>
		    <?php endif; ?>
                </tbody>
            </table>
            <div class="form-actions">
                <input type="hidden" name="add_collection" value="1">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a class="btn" href="<?php echo base_url(); ?>admin/collections">Cancelar</a>
            </div>
        </form>
    </div>
</div>
<script src="<?php echo get_option('path_admin'); ?>js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/messages_es.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="<?php echo get_option('path_admin'); ?>js/admin_scripts.js" type="text/javascript"></script>