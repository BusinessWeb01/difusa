<?php get_header();?>
<br /><br />
<div class="container">
	<div class="form-group">
		<form id="form_register" name="form_register" method="post" action="<?php echo base_url(); ?>registra_controller/registrar/" autocomplete="off"> <!--controlador funcion-->
		<?php if(isset($error)): ?>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <p class="bg-danger">Verifique que las contraseñas coincidan.</p>
            </div>
        </div>
        <?php endif; ?> 
        <?php if(isset($error2)):?>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <p class="bg-danger">Correo ya registrado.</p>
            </div>
        </div>
        <?php endif; ?> 
        <div class="col-xs-4" id="cliente">
        <legend>Datos del cliente:</legend>
			<div class="row">			
				<label>Nombre: </label>
				<input type="text" class="form-control control-contact" maxlength="80" name="client_name" placeholder="Nombre" required>
			</div>
			<div class="row">
				<label>Apellido Paterno: </label>
				<input type="text" class="form-control control-contact" maxlength="100" name="last_name_client" placeholder="Apellido Paterno" required>
			</div>
			<div class="row">
				<label>Apellido Materno: </label>
				<input type="text" class="form-control control-contact" maxlength="100" name="last_name2_client" placeholder="Apellido Materno"><br>
			</div>			
			<legend>Domicilio </legend>
			<div class="row">
				<label>Estado: </label>
				<input type="text" class="form-control control-contact" name="country" id="country" maxlength="40" placeholder="Estado" required>
				<label>Municipio: </label>
				<input type="text" class="form-control control-contact" name="delegation" id="delegation" maxlength="40" placeholder="Municipio" required>
				<label>Colonia: </label>
				<input type="text" class="form-control control-contact" name="suburb" id="suburb" maxlength="40" placeholder="Colonia" required>
				<label>Ciudad: </label>
				<input type="text" class="form-control control-contact" name="locality" id="locality" maxlength="40" placeholder="Localidad" required>
				<label>Calle: </label>			
				<input type="text" class="form-control control-contact" name="street" id="street" maxlength="25" placeholder="Calle" required>
				<label>N° exterior: </label>
				<input type="text" class="form-control control-contact" name="outdoor" id="outdoor" maxlength="10" placeholder="N° exterior" required>
				<label>N° interior: </label>
				<input type="text" class="form-control control-contact" name="indoor" id="indoor" maxlength="10" placeholder="N° interior">
				<label>C&oacute;digo Postal: </label>
				<input type="text" class="form-control control-contact" name="zipcode" id="zipcode" onkeypress="return Numeros(event);" maxlength="5" placeholder="C.P." required>
				<br>
			</div>
			<legend>Contacto</legend>
			<div class="row">
				<label>N&uacute;mero de tel&eacute;fono: </label>
				<input type="text" class="form-control control-contact" id="local" name="local" maxlength="12" placeholder="Local" required>
				<label>N&uacute;mero de celular: </label>
				<input type="text" class="form-control control-contact" id="celular" name="celular" maxlength="15" placeholder="Celular" required><br>				
			</div>
			<legend>Datos de sesi&oacute;n </legend>
			<div class="row">
				<label>Correo: (Tu usuario para iniciar sesi&oacute;n)</label>
				<input type="text" class="form-control control-contact" id="usuario" maxlength="55" name="usuario" placeholder="Usuario" required>
			</div>			
			<div class="row">
				<label>Contraseña: </label>
				<input type="password" class="form-control control-contact" id="password" minlength="8" name="password" placeholder="Contraseña" required>
			</div>
			<div class="row">
				<label>Verificar contraseña: </label>
				<input type="password" class="form-control control-contact" id="password2" minlength="8" name="password2" required><br>
			</div>			
			<br><button type="submit" name="enviar" id="enviar"  class="btn btn-danger">Enviar</button>
		</div>			
		<div class="col-xs-4" id="factura">
			<legend>Domicilio de facturación</legend>
			<div class="row">
				<input type="checkbox" name="copy" onchange="copyinfo(this);">&nbsp;El domicilio de facturaci&oacute;n es el mismo que el de envio.
				<label>Estado: </label>
				<input type="text" class="form-control control-contact" name="country2" id="country2" maxlength="40" placeholder="Estado" required>
				<label>Municipio: </label>
				<input type="text" class="form-control control-contact" name="delegation2" id="delegation2" maxlength="40" placeholder="Delegacion" required>
				<label>Colonia: </label>
				<input type="text" class="form-control control-contact" name="suburb2" id="suburb2" maxlength="40" placeholder="Colonia" required>
				<label>Ciudad: </label>
				<input type="text" class="form-control control-contact" name="locality2" id="locality2" maxlength="40" placeholder="Localidad" required>
				<label>Calle: </label>
				<input type="text" class="form-control control-contact" name="street2" id="street2" maxlength="25" placeholder="Calle" required>
				<label>N° exterior: </label>
				<input type="text" class="form-control control-contact" name="outdoor2" id="outdoor2" maxlength="10" placeholder="N° exterior" required>
				<label>N° interior: </label>
				<input type="text" class="form-control control-contact" name="indoor2" id="indoor2" maxlength="10" placeholder="N° interior" >
				<label>C&oacute;digo Postal: </label>
				<input type="text" class="form-control control-contact" name="zipcode2" id="zipcode2" maxlength="5" placeholder="C.P." required>
				<label>RFC: </label>
				<input type="text" class="form-control control-contact" name="rfc" id="rfc" maxlength="14" placeholder="RFC" required>
				<br>
			</div>
			<legend>Contacto </legend><br>
			<div class="row">
				<label>N&uacute;mero de tel&eacute;fono: </label>
				<input type="text" class="form-control control-contact" id="local2" name="local2" maxlength="12" placeholder="Local" required>
				<label>N&uacute;mero de celular: </label>
				<input type="text" class="form-control control-contact" id="celular2" name="celular2" maxlength="15" placeholder="Celular" required>
				<label>Correo Electronico: </label>
				<input type="text" class="form-control control-contact" id="email2" name="email2" maxlength="55" placeholder="E-mail"><br>
			</div>
			<legend>Suscripciones: </legend>
			<div class="row">
				<input type="checkbox" name="suscribir" onchange="muestraSuscripcion(this);" id="suscribeme">&nbsp;Suscribirse a algun boletin de noticias
			</div><br>
			<div id="suscribe">
			<?php
				for($i=0;$i<count($news);$i++)
				{
					?>
					<div class="col-ms-4 col-xs-offset-0">
						<div class="form-group">
					<?php
					echo "<input type='checkbox' name='newsuser[]' value ='".$news[$i]['id_subscription']."'>&nbsp;".$news[$i]['subscription_name']."";
					?>
						</div>
					</div>
					<?php
				}
			?>
			</div>
		</div>		
		</form>
	</div>
</div><br>
<?php get_footer(); ?>