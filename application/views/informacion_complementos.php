<?php get_header();?>
<!--informacion de producto clickeado para usuarios sin cuenta (al público)-->
<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />
<?php //print_r($this->session->all_userdata());?>
<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion"></h1>
        </div>
    </div>
</div>
<br /><br />
<div class="container">
    <div class="row hidden-xs">
        <div class="col-md-7 col-md-offset-0 col-sm-5 col-sm-offset-0">
            <h2 class="text-left text-top-product"><?php echo $info[0]['product_name']?></h2>
        </div>
        <div class="col-md-5 col-md-offset-0 col-sm-7 col-sm-offset-0">
            <h2 class="text-right text-top-product">Código: <?php echo $info[0]['product_code'];?></h2>
        </div>
        


    </div>
    <div class="row visible-xs">
        <div class="col-xs-10 col-xs-offset-1">
            <h2 class="text-center text-top-product"><?php echo $info[0]['product_name']?></h2>
        </div>
        <div class="col-xs-10 col-xs-offset-1">
            <h2 class="text-center text-top-product">Código: <?php echo $info[0]['product_code'];?></h2>
        </div>
        <div class="col-xs-10 col-xs-offset-1 text-center">
             <a href="#" onclick="history.back();"><p>&lt; Volver</p></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 col-md-offset-0 col-sm-12 col-sm-offset-0">
            <div class="row hidden-xs">
                <div class="col-md-2 col-md-offset-0 col-sm-2 col-sm-offset-0">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                             <a href="#" onclick="history.back();" class="link-volver-in-product"><p>&lt; Volver</p></a>
                        </div>
                    </div>
                    <!-- I-carousel de más imagenes -->
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                                
   
                        </div>
                    </div>
                    <!-- F-carousel de más imagenes -->
                </div>
                <div class="col-md-10 col-md-offset-0 col-sm-7 col-sm-offset-3 col-xs-12 col-xs-offset-0">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 top-img-the-now-ring">
                            <img id="the_img_ring_now" src="<?php echo base_url();echo $info[0]['url_images'];?>"  class="img-responsive size-product-principal center-img" style="width: 440px;height: 500px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row visible-xs">
                <div class="col-xs-10 col-xs-offset-1 top-img-the-now-ring">
                    <div id="carousel-more-products-xs" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
                
                <div class="item active">
            <img src="<?php echo base_url();echo $info[0]['url_images'];?>" alt="CAMIL" class="img-responsive size-product-principal center-img" style="width: 400px;">
        </div>
                
            </div>
    <!-- Controls -->
    
</div>                </div>
            </div>
        </div>
        <div id="demo-container" class="col-md-5 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
            <div class="row visible-sm">
                <br>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
<!--ndp 20151219 -  Before Price:0-->
                    <h3 class="text-price-product">
                        Precio: <span id="the_price_now" class="number-price-product">$<?php echo number_format($info[0]['price'], 2, '.', ',')." ";?><?php echo $info[0]['currency_name'];?></span>
                    </h3>
                    <h4 class="text-price-product">
                        Presentación: <span id="the_price_now" class="number-price-product"><?php echo $info[0]['presentation'];?></span>
                    </h4>
                    <h4 class="text-price-product">
                        Marca: <span id="the_price_now" class="number-price-product"><?php echo $info[0]['brand'];?></span>
                    </h4>
                    <h4 class="text-price-product">
                       País: <span id="the_price_now" class="number-price-product"><?php echo $info[0]['country_name'];?></span>
                    </h4>
                </div>
            </div>
            
            <!-- I-descripción -->
            <br>
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                    <h4 class="text-description-product">Descripción</h4>
                    <div class="row">
                        <div class="col-md-12 col-md-offset-0 line-for-description_product"></div>
                    </div>
                </div>
            </div>
            <div class="row margin-top-for-description">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                    <p class="text-justify text-content-description">
                       <?php echo $info[0]['product_description'];?>
                    </p>
                </div>
            </div>
            <!-- F-descripción -->
            <!-- I-asesoría -->
            <br>
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                    <a href="<?php echo base_url();echo $info[0]['url_techsheets'];?>" target="_blank" class="a-solicitar-asesoria">
                        <h3 class="btn-solicitar-asesoria text-center">
                        <img src="<?php echo base_url();?>img/ico_pdf.png"> &nbspFicha Técnica
                        </h3>
                    </a>
                </div>
            </div>
            <!-- F-asesoría -->
            <form action="<?php echo base_url(); ?>articulos_complementos_controller/agregarProducto"  method="post" name="form1">
            <input type="hidden" name="idproduct" value="<?php echo  $info[0]['id_product']; ?>">
            <input type="hidden" name="nombre" value="<?php echo  $info[0]['product_name']; ?>">
            <input type="hidden" name="codigo" value="<?php echo  $info[0]['product_code']; ?>">
            <input type="hidden" name="presentacion" value="<?php echo  $info[0]['presentation']; ?>">
            <input type="hidden" name="precio" value="<?php echo  $info[0]['price']; ?>">
            <input type="hidden" name="moneda" value="<?php echo  $info[0]['currency_name']; ?>">
            <center><h3 style="color:#6C6D71;">Cantidad:</h3> <input type="number" name="cantidad" min="1" style="width:80px;"></center>
            </form>
            <br>
            <div class="row">
            
                <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                 <a href="#" onclick="javascript:document.form1.submit();" class="a-solicitar-asesoria">
                    <button id="btn_add_product_in_cart" type="submit" class="btn btn-enviar-a-lista btn-block" form="none">+ Añadir a mi carrito</button></a>
                </div>
            </div>
            <!-- I-btn-lista -->
            <br>
            <?php if(isset($wish)){?>
            <div class="row">
            <form action="<?php echo base_url(); ?>articulos_complementos_controller/insertWish"  method="post" name="form2">
             <input type="hidden" name="idproduct" value="<?php echo  $info[0]['id_product']; ?>">
            </form>
                <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                 <a href="#" onclick="javascript:document.form2.submit();" class="a-solicitar-asesoria">
                    <button id="btn_add_product_in_cart" type="submit" class="btn btn-enviar-a-lista btn-block" form="none"><img src="<?php echo base_url();?>img/wish.gif"/>&nbsp;&nbsp;+ Lista de deseos</button></a>
                </div>
            </div><?php }?>
            <br><br><br><br><br>
            <!-- F-btn-lista -->
        </div>
        <div>
        <legend>Comentarios</legend>
            <?php if(!isset($banderaComentario))?>
                <label>No hay comentarios</label>
                <?php else{?>
                        
                    <?php }?>
            <form action="<?php echo base_url()?>articulos_complementos/controller/comenta" method="post">
            <br>
            <input type="hidden" name="userComment" value="<?php echo $this->session->userdata('id_client');?>">
            <textarea name="comment" id="comment" cols="80" rows="5" placeholder="Coloca tu comentario..."></textarea><br><br>
            <button type="submit" name="send" class="btn btn-danger btn-md">Agregar comentario</button><br><br>
            </form>
        </div>
    </div>
</div>
<?php
    get_footer();
?>