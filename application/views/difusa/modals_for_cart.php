<div class="container">
  <!--ndp 20150716 - rfc -->
  <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_rfc_error" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Por favor, ingrese un RFC valido (letras los 4 primeros caracteres, números los siguientes 6 caracteres y 2 ó 3 letras ó números para la homoclave).</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->

    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_missing_fields" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Por favor, debe de llenar los campos obligatorios</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_email_error" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Por favor, debe de registrar un Email valido</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_phone_error" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Por favor, debe de registrar un Teléfono y celular validos</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_missing_shipping_data" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Por favor, debe de llenar todos los campos de envío</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_missing_payment_option" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Por favor, debe de seleccionar un método de pago</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_missing_terms" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Por favor, debe de aceptar los términos y condiciones</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
    
    <div class="row"><!-- modal  -->
        <div class="col-md-12 col-md-offset-0">
            <div id="modal_opcion_factura" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p>¿Desea Facturar?</p>
                        </div>
                        <div class="modal-footer">
                            <button id="btn_no_factura" type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                            <button id="btn_si_factura" type="button" class="btn btn-primary" data-dismiss="modal">SI</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /modal -->
    
    
</div>
