<?php if(isset($product)): ?>
<div id="c_<?php echo $product->slug; ?>" class="carousel slide carousel-product" data-ride="carousel" style="display: none;">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php $contador = 0; foreach($product->images as $value): ?>
        <div class="item <?php if($contador == 0){ echo 'active'; } ?>">
            <a href="<?php echo $product->link; ?>" class="link-img-carousel link-container-ring">
            <img src="<?php echo $product->path_img.$value; ?>" alt="<?php echo $product->product_name.' '.$product->product_code.'-'.$contador; ?>" class="img-responsive center-img size-img-product-cate"/>
            </a>    
        </div>
        <?php $contador++; endforeach; ?>
    </div>
</div>
<?php endif; ?>