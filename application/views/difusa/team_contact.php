<?php get_header(); ?>
<?php //load_view('carousel_top', $carousel_config); ?>
<br /><br />
<?php load_view('section_title', array('text' => 'Contacto')); ?>
<br />
<div class="container">
    <form id="form_contact" name="form_contact" method="post" action="<?php echo base_url();?>contacto_controller/getDataComment">
        <?php if(isset($error)): ?>
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <p class="bg-danger">Ha habido un error al enviar su correo</p>
            </div>
        </div>
        <?php endif;?>
        <div class="row">
            <div class="col-md-4 col-md-offset-0">
                <div class="form-group">
                    <label class="label-contact" for="name">Nombre:</label>
                    <input type="text" class="form-control control-contact" id="name" name="name" onkeypress="return val(event);" required>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-0">
                <div class="form-group">
                    <label class="label-contact" for="phone">Teléfono:</label>
                    <input type="text" class="form-control control-contact" id="phone" name="phone">
                </div>
            </div>
            <div class="col-md-4 col-md-offset-0">
                <div class="form-group">
                    <label class="label-contact" for="email">Email:</label>
                    <input type="email" class="form-control control-contact" id="email" name="email" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-0">
                <div class="form-group">
                    <label class="label-contact" for="email">Asunto:</label>
                    <input type="text" class="form-control control-contact" id="asunto" name="asunto" required>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-0">
                <label class="label-contact" for="comment">Mensaje:</label>
                <textarea class="form-control control-contact" rows="4" id="comment" minlength="15" name="comment" required></textarea>
                <br />
                <div class="row">
                    <div class="col-md-6 col-md-offset-0">
                        <input type="hidden" name="contact_act" value="1" />
                        <button type="submit" class="btn btn-primary btn-lg btn-block btn-contac-form" data-target="#popupTeamContact" data-toggle="modal">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
<div class="modal fade" id="popupTeamContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content popups">
            <div class="modal-header">
                <h4 class="modal-title" id="popupTeamContact">Su mensaje ha sido enviado exitosamente</h4>
            </div>
            <div class="modal-body">
                <p>Te invitamos a conocer a los especialistas en equipo</p>
                <p><a href="http://www.ingenieriatizayuca.com.mx" target="_blank">www.ingenieriatizayuca.com.mx</a></p>
                <p>Equipos y embotelladoras</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
            <input type="hidden" name="tipo" value="equipo">
        </div>
    </div>    
</div>
</form>
</div>


<br/><br/>
<?php get_footer(); ?>