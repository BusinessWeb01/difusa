<footer>
    <div class="row color-footer">
        <div class="container">
			<div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <h5 class="text-center texto-copyright">
						División Industrial de Físicos Unidos S.A. de C.V. 14 de agosto #55 Col. Manuel Avila Camacho, Naucalpan, Estado de México, C.P.53910
                    </h5>
                </div>
            </div>
			<div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <h5 class="text-center texto-copyright">
						<ul class="nav nav-pills nav-stacked nav-footer">
							<li role="presentation"><a href="<?php echo base_url(); ?>contacto">Teléfono: (55) 5293-1292</a></li>
							<li role="presentation"><a href="<?php echo base_url(); ?>contacto">contacto@difusa.com.mx</a></li>
						</ul>
                    </h5>
                </div>
			</div>
			<?php $facebook = get_option('facebook');
				if($facebook): ?>				
				<div class="row">
					<div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
						<a href="<?php echo $facebook; ?>" target="_blank">
							<img src="<?php echo get_option('path_template'); ?>img/facebook-icon.png" width="55px" height="55px" alt="Facebook difusa" class="img-responsive hidden-xs center-img"/>
						</a>
						<div class="row visible-xs">
							<div class="col-xs-12 col-xs-offset-0">
								<a href="<?php echo $facebook; ?>" target="_blank">
									<img src="<?php echo get_option('path_template'); ?>img/facebook-icon.png" width="55px" height="55px" alt="Facebook difusa" class="img-responsive center-img" />
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
                <div class="col-md-3 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2 padding-0">
                    <ul class="nav nav-pills nav-stacked nav-footer text-center">
                        <li role="presentation"><a href="<?php echo base_url(); ?>aviso-de-privacidad">Aviso de Privacidad</a></li> 
                    </ul>
					<br/>
                </div>
				<div class="col-md-3 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2 padding-0">
                    <ul class="nav nav-pills nav-stacked nav-footer text-center">
                        <li role="presentation"><a href="<?php echo base_url(); ?>terminos-y-condiciones">Términos y Condiciones</a></li>
                    </ul>
					<br/>
                </div>
                <div class="col-md-3 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2 padding-0">
                    <ul class="nav nav-pills nav-stacked nav-footer text-center">
                        <li role="presentation"><a href="<?php echo base_url(); ?>politicas-de-envio">Políticas de Envío</a></li> 
                    </ul>
					<br/>
                </div>
                <div class="col-md-3 col-md-offset-0 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2 padding-0">
                    <ul class="nav nav-pills nav-stacked nav-footer text-center">
                        <li role="presentation"><a href="<?php echo base_url(); ?>politicas-de-devolucion">Políticas de Devolución</a></li>
                    </ul>
					<br/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                    <h5 class="text-center texto-copyright">
						<br>© 2015, Difusa | Todos los Derechos Reservados
                    </h5>
                    <br />
                </div>
            </div>
        </div>
    </div>    
</footer>