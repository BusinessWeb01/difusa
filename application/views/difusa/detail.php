<?php $referrer = $this->session->userdata('load_in_title_ring');
$load_in_title = false;
if($referrer){
    if($referrer == 1){
        $load_in_title = true;
    }
}
get_header(); ?>

<br /><br />
<div <?php if($load_in_title){ echo 'id="load_in_title_ring"';} ?> class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion"><?php echo $first_title; ?></h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div id="demo-container" class="col-md-5 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
            <div class="row visible-sm">
                <br />
            </div>
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0">
                    <h3 class="text-price-product">
                        Precio: <span id="the_price_now" class="number-price-product">$<?php echo format_number($product_detail[0]->price); ?></span>
                    </h3>
                </div>
            </div>
            <div class="row"><!-- I-form -->
                <form id="form_add_product" name="form_add_product" method="post" action="<?php echo base_url();?>agregar-producto">
                    <div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0">
                        
                        <div class="row"><!-- I-Campo-cantidad -->
                            <div class="col-md-4 col-md-offset-0 col-sm-4 col-sm-offset-0">
                                <label class="form-control-label" for="quantity">Cantidad:</label>
                            </div>
                            <div class="col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-0">
                                <input type="text" value="1" id="quantity" name="quantity" class="form-control-ring-qty"/>
                                <a id="more_quantity" href="#" class="plus-for-qty">+</a>
                            </div>
                        </div><!-- F-Campo-cantidad -->
                    </div>
                    
                    <input type='hidden' value='1' name='add_product' />
                    <input type='hidden' value='<?php echo $product_detail[0]->product_code; ?>' name='product_code' />
                    <input id="path_images" type='hidden' value='' name='path_images' />
                    <input type='hidden' value='' id='next_option' name='next_option' />
                    <input type='hidden' value='' name='link_return' />
                    <input id="product_price" type='hidden' value='<?php echo $product_detail[0]->price; ?>' name='price' />
                </form>
            </div><!-- F-form -->
            <br />
            <div class="row">
                <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                    <button id="btn_add_product_in_cart" type="submit" class="btn btn-enviar-a-lista btn-block" form="none">+ Agregar al carrito</button>
                </div>
            </div>
            <!-- F-btn-lista -->
        </div>
    </div>
</div>
<?php $modal = array('img_ring' => '', 'name_ring' => $product_detail[0]->product_name, 'model_ring' => $product_detail[0]->product_code, 'price_ring' => $product_detail[0]->price);
load_view('modal_add_product_in_cart', $modal); ?>
<br /><br />
<br />
<?php get_footer(); ?>
<script src="<?php echo get_option('path_template'); ?>js/script.difusa.js"></script>