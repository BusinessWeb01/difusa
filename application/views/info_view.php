<div>
<br><br><br>
	<legend>Datos personales: </legend>	
	<form action="<?php echo base_url()?>panel_controller/update/" method="post"  role="form" class="form-contact" autocomplete="off">
	<div class="form-group">		
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Nombre: </label>
				<input type="text" name="nombre" class="form-control" value="<?php echo$info[0]['client_name'] ?>">
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Apellido paterno: </label>
				<input type="text" name="a_paterno" class="form-control" value="<?php echo$info[0]['last_name_client'] ?>">
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Apellido materno: </label>
				<input type="text" name="a_materno" class="form-control" value="<?php echo$info[0]['last_name2_client'] ?>">
			</div>
		</div>
		<legend>Domicilio</legend>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Estado: </label>
				<input type="text" class="form-control" name="country" maxlength="40" placeholder="Estado" value="<?php echo$info[0]['state'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Municipio: </label>
				<input type="text" class="form-control" name="delegation" maxlength="40" placeholder="Delegacion" value="<?php echo$info[0]['delegation'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Colonia: </label>
				<input type="text" class="form-control" name="suburb" maxlength="40" placeholder="Colonia" value="<?php echo$info[0]['suburb'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Ciudad: </label>
				<input type="text" class="form-control" name="locality" maxlength="40" placeholder="Localidad" value="<?php echo$info[0]['locality'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Calle: </label>			
				<input type="text" class="form-control" name="street" maxlength="25" placeholder="Calle" value="<?php echo$info[0]['street'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">N° exterior: </label>
				<input type="text" class="form-control" name="outdoor" maxlength="10" placeholder="N° exterior" value="<?php echo$info[0]['outdoor_number'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">N° interior: </label>
				<input type="text" class="form-control" name="indoor" maxlength="10" placeholder="N° interior" value="<?php echo$info[0]['indoor_number'] ?>">
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">C&oacute;digo Postal: </label>
				<input type="text" class="form-control" name="zipcode" maxlength="5" placeholder="C.P." value="<?php echo$info[0]['zipcode'] ?>" required>
			</div>
		</div>
		<legend>Contacto</legend>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">N&uacute;mero de tel&eacute;fono: </label>
				<input type="text" class="form-control" name="local" placeholder="Telefono" value="<?php echo $info[0]['phone_number'] ?>">
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">N&uacute;mero de celular:</label>
				<input type="text" class="form-control" name="celular" placeholder="Celular" value="<?php echo $info[0]['cellphone_number'] ?>" >
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Correo: </label>
				<input type="text" class="form-control" name="usuario" maxlength="5" placeholder="Correo" value="<?php echo$info[0]['contact_mail'] ?>" required>
			</div>
		</div>
		<legend>Datos de Facturaci&oacute;n</legend>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Estado: </label>
				<input type="text" class="form-control" name="country2" maxlength="40" placeholder="Estado" value="<?php echo$info[0]['state_bussiness'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Municipio: </label>
				<input type="text" class="form-control" name="delegation2" maxlength="40" placeholder="Delegacion" value="<?php echo$info[0]['delegation_bussiness'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Colonia: </label>
				<input type="text" class="form-control" name="suburb2" maxlength="40" placeholder="Colonia" value="<?php echo$info[0]['suburb_bussiness'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Ciudad: </label>
				<input type="text" class="form-control" name="locality2" maxlength="40" placeholder="Localidad" value="<?php echo$info[0]['locality_bussiness'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Calle: </label>			
				<input type="text" class="form-control" name="street2" maxlength="25" placeholder="Calle" value="<?php echo$info[0]['street_bussiness'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">N° exterior: </label>
				<input type="text" class="form-control" name="outdoor2" maxlength="10" placeholder="N° exterior" value="<?php echo$info[0]['outdoor_number_bussiness'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">N° interior: </label>
				<input type="text" class="form-control" name="indoor2" maxlength="10" placeholder="N° interior" value="<?php echo$info[0]['indoor_number_bussiness'] ?>">
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">C&oacute;digo Postal: </label>
				<input type="text" class="form-control" name="zipcode2" maxlength="5" placeholder="C.P." value="<?php echo$info[0]['zipcode_bussiness'] ?>" required>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">RFC: </label>
				<input type="text" name="rfc" class="form-control" value="<?php echo$info[0]['rfc'] ?>">
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Correo de la empresa: </label>		
				<input type="text" name="email_bussiness" class="form-control" value="<?php echo$info[0]['email_bussiness'] ?>">
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-0">
			<div class="form-group">
				<label class="control-label">Telefono de la empresa: </label>		
				<input type="text" name="phone_number_bussiness" class="form-control" value="<?php echo$info[0]['phone_number_bussiness'] ?>">
			</div>
		</div>
		<legend></legend>
		<div class="col-ms-4 col-xs-offset-0">
			<div class="form-group">
				<center><button id="actual" type="submit" name="actualiza" class="btn btn-danger btn-sm">Actualizar informaci&oacute;n</button></center>
			</div>
		</div>		
	</form>
</div>
</div>
		</div>
	</div>
	</div>
</div>
<?php get_footer();?>