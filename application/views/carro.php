<?php get_header(); ?><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <?php
            //si el carrito contiene productos los mostramos
            if ($carrito = $this->cart->contents()) {
                ?>
                <div class="grid_5" id="contenidoCarrito">
                    <table class="carro">
                       <legend>Carrito de la compra</legend>
                        <tr>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Eliminar</th>
                        </tr>
                    <?php
                    $cant=0;
                    foreach ($carrito as $item) {
                        ?>
                        <tr>
                            <td><?= ucfirst($item['name']) ?></td>

                            <td>
                                <?php
                                $nombres = array('nombre' => ucfirst($item['name']));
                                $precio = array();
                                $precio = $item['price'];
                                if ($this->cart->has_options($item['rowid'])) {
                                    foreach ($this->cart->product_options($item['rowid']) as $opcion => $value) {
                                        echo " <em>" . $value . "</em>";
                                    }
                                }
                                ?>
                            </td>
                            <td><?= $item['price'] ?></td>
                            <td><?= $item['qty'] ?></td>
                            <?php $cant=$cant+$item['qty'];?>
                            <!--creamos el enlace para eliminar el producto
                            pulsado pasando el rowid del producto-->
                            <td id="eliminar"><?= anchor('articulos_insumos_controller/eliminarProducto/' . $item['rowid'], 'Eliminar') ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="total">
                        <td><strong>Total:</strong></td>
                        <!--mostramos el total del carrito
                        con $this->cart->total()-->
                        <td colspan="1"><?= $nprod=number_format($this->cart->total(),2)." ".$this->session->userdata('destino_cambio');?> </td>
                        <!--creamos un enlace para eliminar el carrito-->
                        <td  id="eliminarCarrito"><?= anchor('articulos_insumos_controller/eliminarCarrito/', 'Vaciar carrito')?></td>
                        <td ><?php echo $cant;?></td>
                        <td>
                            
        <form name="formTpv" method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr" target="_blank">
   <!--<form name="formTpv" method="post" action="https://www.paypal.com/cgi-bin/webscr">-->
<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="business" value="josh.mf49@gmail.com">
<input type="hidden" name="item_name" value="Compra difusa">
<?php   

        if($carrito = $this->cart->contents())
        {
            $i = 1;
            foreach($carrito as $item)
            {
            ?>
                <input type="hidden" name="item_name_<?=$i?>" value="<?php echo ucfirst($item['name']);?>" />
                <input type="hidden" name="quantity_<?=$i?>" value="<?php echo $item['qty']; ?>">
                <input type="hidden" name="item_number_<?=$i?>" value="<?php echo $item['rowid'];?>" />
                <input type="hidden" name="amount_<?=$i?>" value="<?php echo number_format($item['price'],2); ?>" />
            <?php
            $i++;
            }
        }
        ?>
<input type="hidden" name="page_style" value="primary">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="return" value="http://localhost:8081/difusa/pago/exito"">
<input type="hidden" name="rm" value="2">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="cancel_return" value="http://localhost:8081/difusa/pago/error"">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="currency_code" value="MXN">
<input type="hidden" name="cn" value="PP-BuyNowBF">
<input type="hidden" name="custom" value="">
<input type="hidden" name="first_name" value="<?php echo $this->session->userdata('client_name');?>">
<input type="hidden" name="last_name" value="<?php echo $this->session->userdata('last_name_client');?>">
<input type="hidden" name="address1" value="<?php echo $this->session->userdata('street').' No. Ext.'.$this->session->userdata('outdoor_number').' No. In.'.$this->session->userdata('indoor_number');?>">
<input type="hidden" name="city" value="<?php echo $this->session->userdata('locality');?>">
<input type="hidden" name="zip" value="<?php echo $this->session->userdata('zipcode');?>">
<input type="hidden" name="night_phone_a" value="<?php echo $this->session->userdata('cellphone_number');?>">
<input type="hidden" name="night_phone_b" value="<?php echo $this->session->userdata('cellphone_number');?>">
<input type="hidden" name="night_phone_c" value="">
<input type="hidden" name="lc" value="es">
<input type="hidden" name="country" value="ES">
<input type="submit" value="Comprar">
</form>
                        </td>
                    </tr>
                </table>
                </div>

            <?php
            }else{
            	echo '<h3>No hay artículos en el carrito.</h3>';
            }
            ?>
        </div>
    </div>
</div>

<br /><br />
<div style="margin-top: 300px;"></div>
<?php get_footer(); ?>