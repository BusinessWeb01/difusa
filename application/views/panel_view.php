<?php get_header();?>
<div class="container" id="panelcontainer">
	<div>
		<div class="form-group">
		<div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
					<nav>
                    <ul class="  nav navbar-nav nav navbar-collapse">
						<li role="presentation">
							<a href="<?php echo base_url();?>gral-info">Información de cuenta</a>
						</li> 
						<li role="presentation">
							<!--menu-->
							<a href="<?php echo base_url();?>orders">Pedidos</a>
						</li> 
                        <li role="presentation">
                        	<a href="<?php echo base_url();?>comments">Comentarios de productos</a>
                        </li>
						<li role="presentation" >
							<a href="<?php echo base_url();?>suscription">Boletin de noticias</a>
						</li>
                        <li role="presentation" >
                        	<a href="<?php echo base_url();?>wishes">Lista de deseos</a>
                        </li>
						<li role="presentation">
							<a href="<?php echo base_url();?>refund">Devoluciones</a>
						</li>
                    </ul>
					</nav>
				