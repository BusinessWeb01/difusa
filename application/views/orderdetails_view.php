<?php //print_r($detail); ?>
<div>
<br><br><br>
<legend>Desglose de pedido</legend>
<table class='rowstable'>
	<tr><th>Producto</th><th>Referencia de producto</th><th>Precio</th><th>Cantidad</th><th>Subtotal</th></tr>
	<?php	
		for($i=0;$i<count($detail);$i++)
		{
			echo "<tr>";
			
			if(strcmp($detail[$i]['id_category'],"INS") === 0)
				echo "<td><a href='".base_url()."informacion_insumos/producto/".$detail[$i]['id_product']."'>".$detail[$i]['product_name']."</a></td>";
			elseif(strcmp($detail[$i]['id_category'],"COM") === 0 )
				echo "<td><a href='".base_url()."informacion_complementos/producto/".$detail[$i]['id_product']."'>".$detail[$i]['product_name']."</a></td>";

			echo "<td id='ids'>".$detail[$i]['id_product']."</td>";
			echo "<td> $".$detail[$i]['price'].$detail[$i]['currency_name']."</td>";
			echo "<td>".$detail[$i]['quantity']."</td>";
			echo "<td id='total'> $".$detail[$i]['price_total']."</td>";
			echo "</tr>";
		}	
	?>  
	<tr><td></td><td></td><td></td><td></td><td id="total">Total&nbsp; <?php echo $detail[0]['order_total'] ?></td></tr>
</table><br><br>
<button style="float: right;" type="button" name="Regresa" class="btn btn-danger btn-sm" onclick="history.back();">Regresar</button>
</div>
</div>
</div>
</div>
</div>
<?php get_footer();?>