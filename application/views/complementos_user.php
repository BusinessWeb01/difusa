<!-- Vista de un usuario cualquiera logueado y muestra la lista de los porductos segun sub-categorias de la categoria complementos
$marca-marcas de complementos
$presentacion-presentaciones complementos
$pais-paises de complementos
$cate-sub-categorias
$info-info productos
 -->
<?php get_header();?>

<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />

<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion"></h1>
        </div>
    </div>
</div>
<br /><br />

<div style="text-align:center;margin-bottom:30px;">
  <form name="form_contact" method="post"  action="<?php echo base_url();?>articulos_complementos_controller/filtro/0/8/<?php echo $cant;?>/0/0/0/0/0/1/">
      Cantidad de Productos a Mostrar: 
        <input type="number" class="filtro" name="mostrar" min="1" >
      Marca: 
        <select class="filtro" name="marca" style="margin-right:10px;">
        <?php if (isset($marca1)&&isset($marca2[0]['brand'])) {?>
         <option value="<?php echo $marca1; ?>"><?php echo $marca2[0]['brand']; ?></option>
      <?php  } ?>
            <option value="%"></option>
              <?php
              for($x=0;$x<count($marca);$x++) {
              if ($marca[$x]['id_brand']==$marca[$x-1]['id_brand']) {
              }else{?>
            <option value="<?php echo $marca[$x]['id_brand'];?>"><?php echo $marca[$x]['brand'];?>
            </option>
            <?php }} ?>
        </select>
      Presentación: 
        <select class="filtro" name="presentacion" style="margin-right:10px;">
        <?php if (isset($presen1)&&isset($presen2[0]['presentation'])) {?>
         <option value="<?php echo $presen1; ?>"><?php echo $presen2[0]['presentation']; ?></option>
      <?php  } ?>
            <option value="%"></option>
              <?php
              for($x=0;$x<count($presentacion);$x++) {
              if ($presentacion[$x]['presentation']==$presentacion[$x-1]['presentation']) {
              }else{?>
            <option value="<?php echo $presentacion[$x]['id_presentation'];?>"><?php echo $presentacion[$x]['presentation'];?></option>
            <?php }} ?>
        </select>
      País: 
        <select class="filtro" name="pais" style="margin-right:10px;">
        <?php if (isset($pais1)&&isset($pais2[0]['country_name'])) { ?>
         <option value="<?php echo $pais1; ?>"><?php echo $pais2[0]['country_name']; ?></option>
      <?php  } ?>
          <option value="%"></option>
           <?php
            for($x=0;$x<count($pais);$x++) {
            if ($pais[$x]['country_name_iso3']==$pais[$x-1]['country_name_iso3']) {
            }else{?>
         <option value="<?php echo $pais[$x]['country_name_iso3'];?>"><?php echo $pais[$x]['country_name'];?></option>
          <?php } }?>
        </select>
      Sub-Categoría: 
        <select class="filtro" name="cate" style="margin-right:10px;">
        <?php if (isset($cat1)&&isset($cat2[0]['sub_category'])) {?>
         <option value="<?php echo $cat1; ?>"><?php echo $cat2[0]['sub_category']; ?></option>
      <?php  } ?>
          <option value="%"></option>
            <?php
            for($x=0;$x<count($cate);$x++) {
            if ($cate[$x]['id_sub_category']==$cate[$x-1]['id_sub_category']) {
            }else{?>
          <option value="<?php echo $cate[$x]['id_sub_category'];?>"><?php echo $cate[$x]['sub_category'];?></option>
            <?php } }?>
        </select>
<!--Precio: 
<select class="filtro" name="precio" style="margin-right:10px;">
<option value="%"></option>
<?php/*
for($x=0;$x<count($pais);$x++) {*/?>
  <option value="<?php/* echo $pais[$x]['country_name_iso3'];*/?>"><?php /*echo $pais[$x]['country_name'];*/?></option>
  <?php/* } */?>
</select>-->
    <input class="bfiltrar" type="submit" name="filtro" value="Filtrar">
  </form>
</div>
<br>

<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">
          <?php   $numpr=count($info);
         $total=count($info2);
         if($numpr<1){ }else{ echo $info[0]['sub_category'];}?>
           
         </h1>
        </div>
    </div>
</div>

<br><br><br>

<div style="text-align:center;">
  <?php 
  if ($numpr<8) {
   $numero=$numpr; # code...
  }
## Cuando la cantidad de tuplas devueltas es mayor a la cantidad de productos a mostrar
  $sec=$numpr/$cant;
 for($i=0;$i<$total;$i++) {
  ?>
<form id="form1" name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>informacion_complementos/producto/<?php echo $info2[$i]['id_product'];?>" style="display: inline-block;">

<a class="linkcart" id="iden" href="#" onclick="javascript:document.form<?php echo $i;?>.submit();">
<div class="boxpr">
<img class="imagpr" src="<?php echo base_url();echo $info2[$i]['url_images'];?>"/>
<br>
<br>
Nombre: 
  <?php
  echo $info2[$i]['product_name'];
  ?> 
<br>
Código:
   <?php
  echo $info2[$i]['product_code'];
  ?> 
<br>Marca:
   <?php
  echo $info2[$i]['brand'];
  ?> 
<br>Precio:
   <?php
  echo number_format($info2[$i]['price'], 2, '.', ',');
  ?> 

   <?php
  echo $info2[$i]['currency_name'];
  ?> 
  <br>Presentación:
   <?php
  echo $info2[$i]['presentation'];
  ?> 
  
<br><p class="hidde">Precio descuento:</p>
 <?php
  echo $info2[$i]['before_price'];
  ?>  
  <a class="linkcart" href="#popup<?php echo $i; ?>">
  <div ><img src="<?php echo base_url();?>img/cart.gif"/><br>
    Agregar al carrito de compras</div>
  </a>
</div>
 </a>
 </form>

 <div class="overlay" id="popup<?php echo $i; ?>" >
  <div class="popup">
    <h3>Agregar al carrito</h3><br>
    <a class="close" href="#">&times;</a>
    <div class="content">
     <form action="<?php echo base_url(); ?>articulos_complementos_controller/agregarProducto"  method="post">
     <input type="hidden" name="idproduct" value="<?php echo  $info2[$i]['id_product']; ?>">
                  <h4 class="text-price-product">
                  Nombre: <span id="the_price_now" class="number-price-product"><?php echo  $info2[$i]['product_name']; ?></span>
                    </h4>
                    <h4 class="text-price-product">
                        Presentación: <span id="the_price_now" class="number-price-product"><?php echo  $info2[$i]['presentation']; ?></span>
                    </h4>
                    <h4 class="text-price-product">
                        Código: <span id="the_price_now" class="number-price-product"><?php echo  $info2[$i]['product_code']; ?></span>
                    </h4>
                    <h4 class="text-price-product">
                       Precio: <span id="the_price_now" class="number-price-product"><?php echo  $info2[$i]['price']; ?> <?php echo  $info2[$i]['currency_name']; ?></span>
                    </h4>
      <input type="hidden" name="nombre" value="<?php echo  $info2[$i]['product_name']; ?>">
      <input type="hidden" name="codigo" value="<?php echo  $info2[$i]['product_code']; ?>">
      <input type="hidden" name="presentacion" value="<?php echo  $info2[$i]['presentation']; ?>">
      <input type="hidden" name="precio" value="<?php echo  $info2[$i]['price']; ?>">
      <input type="hidden" name="moneda" value="<?php echo  $info2[$i]['currency_name']; ?>">
      Cantidad: <br><input type="number" name="cantidad" min="1" style="width:80px;">
      <br><br><input type="submit" class="botoncart" value="Agregar">
      </form>
    </div>
  </div>
</div>
  <?php
  }
  ?>

</div>
<div style="text-align:center;">
<?php
$ini2=0;
if ($pais1=="%") {
          $pais1=0;
        }
        
        if ($presen1=="%") {
          $presen1=0;
        }
        if ($marca1=="%") {
          $marca1=0;
        }
if ($numpr>7) {
 for($j=1;$j<=$sec;$j++) {
$numero2=$cant * $j;

if ($pagina==$j) {
  ?>

<a href="<?php echo base_url();?>articulos_complementos_controller/filtro/<?php echo $ini2;?>/<?php echo $numero2?>/<?php echo $cant?>/<?php echo $info2[0]['id_sub_category'];?>/<?php echo $pais1; ?>/<?php echo $presen1; ?>/<?php echo $marca1; ?>/<?php echo $j; ?>"><div class="activo"><?php echo $j; ?></div></a>

<?php }else{ ?>


<a href="<?php echo base_url();?>articulos_complementos_controller/filtro/<?php echo $ini2;?>/<?php echo $numero2?>/<?php echo $cant?>/<?php echo $info2[0]['id_sub_category'];?>/<?php echo $pais1; ?>/<?php echo $presen1; ?>/<?php echo $marca1; ?>/<?php echo $j; ?>"><div class="pag"><?php echo $j; ?></div></a>


<?php
}
$ini2=$ini2 + $cant;
}
$cero=0;
$sec2=($j-1)*$cant;
 $residuo=$numpr-$sec2;
if($residuo!=$cero)
{
  $numero2=$numero2+$residuo;
  if ($pagina==$j) {
  ?>

<a href="<?php echo base_url();?>articulos_complementos_controller/filtro/<?php echo $ini2;?>/<?php echo $numero2?>/<?php echo $cant?>/<?php echo $info2[0]['id_sub_category'];?>/<?php echo $pais1; ?>/<?php echo $presen1; ?>/<?php echo $marca1; ?>/<?php echo $j; ?>"><div class="activo"><?php echo $j; ?></div></a>

<?php }else{ ?>


<a href="<?php echo base_url();?>articulos_complementos_controller/filtro/<?php echo $ini2;?>/<?php echo $numero2?>/<?php echo $cant?>/<?php echo $info2[0]['id_sub_category'];?>/<?php echo $pais1; ?>/<?php echo $presen1; ?>/<?php echo $marca1; ?>/<?php echo $j; ?>"><div class="pag"><?php echo $j; ?></div></a>


<?php
}

}
 # code...
}?>


</div>
<br /><br />

<?php
    get_footer();
?>