<?php get_header();?>

<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
a
{
    color: black;
}
a:hover
{
	color: #D70505;
}
</style>
</head>
<body><br><br>
<div class="container">
	<div class="well nav-collapse sidebar-nav" style="float:left;" id="menu-admin">
		<ul class="nav nav-tabs nav-stacked main-menu">
			<li role="presentation">
				<a href="<?php echo base_url();?>admin_controller/allClients">Clientes</a>
			</li> 
			<li role="presentation">
				<a href="<?php echo base_url();?>admin_controller/orders">Ordenes</a>
			</li> 
           	<li role="presentation">
	           	<a href="<?php echo base_url();?>admin_controller/categories">Categorías</a>
           	</li>
			<li role="presentation" >
				<a href="<?php echo base_url();?>admin_controller/products">Productos</a>
			</li>
           	<li role="presentation" >
	           	<a href="<?php echo base_url();?>admin_controller/topSales">Mas vendidos</a>
    	    </li>
    	    <li role="presentation">
	           	<a href="<?php echo base_url();?>admin_controller/allUsers">Usuarios</a>
    	    </li>
        </ul>
	</div>
		<!--<div style='height:20px;'></div>  -->
    <	div class="col-md-10 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0" style="float:right;">
			<?php echo $output; ?>
    	</div>
    </div>
</body>
<br>
<?php get_footer();?>