<!-- Vista de un usuario sin loguear y muestra la lista de los porductos segun sub-categorias de la categoria promociones
$marca-marcas de promociones
$presentacion-presentaciones promociones
$pais-paises de promociones
$cate-sub-categorias
$info-info productos
 -->
<?php get_header();?>

<?php load_view('carousel_top', $carousel_config); ?>
<br /><br />

<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion"> </h1>
        </div>
    </div>
</div>
<br /><br />
<div style="text-align:center;margin-bottom:30px;">
<form name="form_contact" method="post"  action="<?php echo base_url();?>promociones/filtro">
Cantidad de Productos a Mostrar: <input type="number" name="mostrar" min="1" style="width:80px;margin-right:10px;">
Marca: 
<select name="marca" style="margin-right:10px;">
<option value="%"></option>
<?php
  for($x=0;$x<count($marca);$x++) {
  if ($marca[$x]['id_brand']==$marca[$x-1]['id_brand']) {
    # code...
  }else{?>
  <option value="<?php echo $marca[$x]['id_brand'];?>"><?php echo $marca[$x]['brand'];?></option>
  <?php }} ?>
</select>

Presentación: 
<select name="presentacion" style="margin-right:10px;">
<option value="%"></option>
<?php
for($x=0;$x<count($presentacion);$x++) {
  if ($presentacion[$x]['presentation']==$presentacion[$x-1]['presentation']) {
    # code...
  }else{?>
  <option value="<?php echo $presentacion[$x]['id_presentation'];?>"><?php echo $presentacion[$x]['presentation'];?></option>
  <?php }} ?>
</select>

País: 
<select name="pais" style="margin-right:10px;">
<option value="%"></option>
<?php
for($x=0;$x<count($pais);$x++) {
  if ($pais[$x]['country_name_iso3']==$pais[$x-1]['country_name_iso3']) {
    # code...
  }else{?>
  <option value="<?php echo $pais[$x]['country_name_iso3'];?>"><?php echo $pais[$x]['country_name'];?></option>
  <?php } }?>
</select>

Sub-Categoría: 
<select name="cate" style="margin-right:10px;">
<option value="%"></option>
<?php
for($x=0;$x<count($cate);$x++) {
  if ($cate[$x]['id_sub_category']==$cate[$x-1]['id_sub_category']) {
    # code...
  }else{?>
  <option value="<?php echo $cate[$x]['id_sub_category'];?>"><?php echo $cate[$x]['sub_category'];?></option>
  <?php } }?>
</select>

<!--Precio: 
<select name="precio" style="margin-right:10px;">
<option value="%"></option>
<?php/*
for($x=0;$x<count($pais);$x++) {*/?>
  <option value="<?php/* echo $pais[$x]['country_name_iso3'];*/?>"><?php /*echo $pais[$x]['country_name'];*/?></option>
  <?php/* } */?>
</select>-->
<input class="bfiltrar" type="submit" name="filtro" value="Filtrar">

</form>

</div>
<br>

<div id="load_in_title_section" class="container">
    <div class="row color-barra-seccion">
        <div class="col-md-12 col-md-offset-0">
            <h1 class="text-center texto-barra-seccion">
         <?php   $numpr=count($info);
         if($numpr<1){ }else{ echo $info[0]['sub_category'];}?>
           
         </h1>
        </div>
    </div>
</div>

<br><br><br>
<div style="text-align:center;">


  <?php 

  if($numpr<$cant)
  {
   
   if($numpr<1){echo '<h4>No se encontraron artículos.</h4> <br><br>';}else{ 
    ## Cuando la cantidad de tuplas devueltas es menor a la cantidad de productos a mostrar

    for($i=0;$i<count($info);$i++) {?>
<form id="form1" name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>informacion/producto/<?php echo $info[$i]['id_product'];?>" style="display: inline-block;">

<a class="linkcart" href="#" onclick="javascript:document.form<?php echo $i;?>.submit();">
<div class="boxpr">
<img class="imagpr" src="<?php echo base_url();echo $info[$i]['url_images'];?>"/>
<br>
<br>
Nombre: 
  <?php
  echo $info[$i]['product_name'];
  ?> 
<br>
Código:
   <?php
  echo $info[$i]['product_code'];
  ?> 
<br>Marca:
   <?php
  echo $info[$i]['brand'];
  ?> 
<br><s>Precio:
   <?php
  echo $info[$i]['price'].' ';
  ?> 

   <?php
  echo $info[$i]['currency_name'];
  ?></s>
  <br>Precio descuento:
 <?php
  echo $info[$i]['before_price'];
  ?> 
  <br>Presentación:
   <?php
  echo $info[$i]['presentation'];
  ?> 
  

</div></a>
</form>
      <?php
  }}}else{
## Cuando la cantidad de tuplas devueltas es mayor a la cantidad de productos a mostrar

  $sec=$numpr/$cant;
 for($i=$ini;$i<$numero;$i++) {
  ?>
<form id="form1" name="form<?php echo $i;?>" method="post" action="<?php echo base_url();?>informacion/producto/<?php echo $info[$i]['id_product'];?>" style="display: inline-block;">

<a class="linkcart" href="#" onclick="javascript:document.form<?php echo $i;?>.submit();">
<div class="boxpr">
<img class="imagpr" src="<?php echo base_url();echo $info[$i]['url_images'];?>"/>
<br>
<br>
Nombre: 
  <?php
  echo $info[$i]['product_name'];
  ?> 
<br>
Código:
   <?php
  echo $info[$i]['product_code'];
  ?> 
<br>Marca:
   <?php
  echo $info[$i]['brand'];
  ?> 
<br>Precio:
   <?php
  echo $info[$i]['price'].' ';
  ?> 

   <?php
  echo $info[$i]['currency_name'];
  ?> 
  <br>Presentación:
   <?php
  echo $info[$i]['presentation'];
  ?> 
  
<br>Precio descuento:
 <?php
  echo $info[$i]['before_price'];
  ?> 
</div>
 </a>
 </form>
  <?php
  }
  ?>

</div>
<div style="text-align:center;">
<?php
$ini2=0;

for($j=1;$j<=$sec;$j++) {
$numero2=$cant * $j;

  ?>


<a href="<?php echo base_url();?>promociones/paginas?ini=<?php echo $ini2;?>&numero=<?php echo $numero2?>&cant=<?php echo $cant?>&cat=<?php echo $info[0]['id_sub_category'];?>"><div class="pag"><?php echo $j; ?></div></a>

<?php
$ini2=$ini2 + $cant;
}
$cero=0;
$sec2=($j-1)*$cant;
 $residuo=$numpr-$sec2;
if($residuo!=$cero)
{
  $numero2=$numero2+$residuo;
  ?>

  <a href="<?php echo base_url();?>promociones/paginas?ini=<?php echo $ini2;?>&numero=<?php echo $numero2?>&cant=<?php echo $cant?>&cat=<?php echo $info[0]['id_sub_category'];?>"><div class="pag"><?php echo $j; ?></div></a>

<?php
}}
?>

</div>
<br /><br />

<?php
    get_footer();
?>