<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('carousel');
	}

	public function index(){
		$send = array(
			'carousel_config' => $this->carousel->home(),
			'section_nav' => 'home'
		);
		$s = $this->session->userdata('origen');
		if($s == NULL)
		{
			$monedas = array(
				'origen' => 'Peso Mexicano',
				'destino_cambio' => 'MXN'
				);
			$this->session->set_userdata($monedas);
		}
		load_view('home', $send);
	}

	public function cart(){
		$this->load->library('lb_cart');
		$send = $this->lb_cart->get_content();
		load_view('cart', $send);
	}	
	
	public function contact(){
		$this->load->library('lb_contact');
		$send = $this->lb_contact->get_variables($this->input->post('contact_act'));
		if($send){
			load_view('contact', $send);
		}
		else{
			redirect('gracias');
		}
	}
	
	public function error_404(){
		load_view('error_404');
	}
		
	public function search(){
		$this->load->library('lb_search');
		$send = $this->lb_search->get_content();
		load_view('search', $send);
	}
	
	public function thanks(){
		$send = array(
			'carousel_config' => $this->carousel->thanks()
		);
		load_view('thanks', $send);
	}
	
	public function select($url = ''){
		$this->load->library('lb_pages');
		$send = $this->lb_pages->get_page($url);
		if($send){
			load_view('page', $send);
		}
		else{
			load_view('error_404');
		}
	}
	
	public function ingredients(){
		$send = array(
			'carousel_config' => $this->carousel->ingredients()
		);
		load_view('ingredients',$send);
	}
	
	public function products($product_type='',$category='',$sub_category='',$page=1){
		$this->load->library('lb_products');
		$send = $this->lb_products->get_content($product_type,$category,$sub_category,$page);
		load_view($send['page'],$send);
	}
	
	public function products_category($product_type='',$category='',$page=1){
		$this->products($product_type,$category,'',$page);
	}
	
	public function products_sub_category($product_type='',$category='',$sub_category='',$page=1){
		$this->products($product_type,$category,$sub_category,$page);
	}
	
	public function products_type($product_type='',$page=1){
		$this->products($product_type,'','',$page);
	}
	
	public function detail($product_code='',$slug=''){
		$this->load->library('lb_products');
		
		$send = array(
			'page' => 'detail',
			'product_detail' => $this->lb_products->get_product_detail($product_code,$slug),
			'first_title' => 'Detalle de producto',
		);
		load_view($send['page'],$send);
	}
	
	public function summary_cart(){
		$this->load->library('lb_summary_cart');
		$send = $this->lb_summary_cart->get_content();
		load_view('summary_cart', $send);
	}
	
		function thanks_payment(){
		$this->load->model('md_invoicing');
		$exists = false;
		$post = $this->input->post();
		$get = $this->input->get();

               //ndp 20150730 - boton de pruebas
               $CI = &get_instance();
               $CI->load->library('session');
               $session_user = $CI->session->all_userdata();
                //$temp = $CI->session->__get('folio');
               //log_message('debug','$session_user[folio]; = '.print_r($session_user['folio'],TRUE));

               /**
                * HARD CODE PARA PRUEBAS
                */
                //ndp 20150716 - hardcode para obtener una respuesta de vuelta
		
                $post = array(
                        'codigo' => 0,
                        'mensaje' => 'Pago exitoso',
                        'autorizacion' => 856866,
                        'referencia' => 55,
                        'referencia' => $session_user['folio'],
                        'importe' => '0.01',
                        'mediopago' => 'TDC',
                        'financiado' => 'REV',
                        'plazos' => 0,
                        's_transm' => 200040514,
                        'hash' => '20c7cfb4718251643faef988ed3b7e336a1e8b16a77e5a262ecf6db1f8d105b4',
                        //'tarjetahabiente' => 'DAVID ALDRIN GALVAN SANDOVAL'
                        'tarjetahabiente' => 'USUARIO DE PRUEBA'
		);
                
                /*
                $get = array(
                        't' => 20150811085415,
                        'f' => 102//$session_user['folio']
                );
		*/
		//ndp 20150715 - Haciendo pruebas
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
		log_message('debug', '$GET='.print_r($get, TRUE));		
		log_message('debug', '$POST='.print_r($post, TRUE));		
		log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');
		
		$session_user = $this->session->all_userdata();
		$data = array(
			'post' => $post,
			'get' => $get,
			'session_user' => $session_user
		);
		//ndp 20150715 - Haciendo pruebas
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
		log_message('debug', '$data array='.print_r($data, TRUE));		
		log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');
		
		$data = json_encode($data);

		
		//ndp 20150715 - Haciendo pruebas
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
		log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
		log_message('debug', '$data json='.print_r($data, TRUE));		
		log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');



                 $desplegarPantallaExito = 'no';
                 $carrito = null;//arreglo para el carrito en la pagina de gracias

                //ndp 20150811 - solo funciona para paypal, para la url de retorno
		if($get){
			//ndp 20150715 - Haciendo pruebas
			log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');		
			log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION: function thanks_payment() XXXXXXXXXXXXXXXXXX');		
			log_message('debug', 'ES GET');		
			log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');

			if(isset($get['t'])){
				if(isset($get['f'])){
					$this->load->library('lb_functions');
					$time = $this->lb_functions->get_the_time($get['t']);
					$this->load->model('md_pages');
					//$payment = $this->md_pages->get_info_of_payment($time, $get['f']);
                                        $payment = $this->md_pages->get_info_of_payment_by_folio($get['f']);


					//ndp 20150715 - Haciendo pruebas
					log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION get[t] get[f]: application\controllers\pages.php XXXXXXXXXXXXXXXXXX');
					log_message('debug', 'XXXXXXXXXXXXXXXXXX UBICACION get[t] get[f]: function thanks_payment() XXXXXXXXXXXXXXXXXX');
					log_message('debug', '$payment='.print_r($payment, TRUE));		
					log_message('debug', '|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||');
					
					if($payment){
						$this->md_invoicing->update_payment($data, $payment->id_pago);
						$this->lb_functions->create_file_for_sap($payment);
						$exists = true;
                                                $desplegarPantallaExito = 'si';

                                                $info_pago = $this->md_invoicing->get_informacion_pago($get['f']);
                                                $carrito = json_decode($info_pago['informacion'],TRUE);


					}



				}
			}else{

                            //error en el pago
                        }
		}
                ////////////////////////////////////////////////SE AGREGA ELSE IF
                else if($post){

		log_message('debug', '...post');
			if(isset($post['referencia'])){
			log_message('debug', '...referencia');
				$codigo = 999;
				if(isset($post['codigo'])){
					$codigo = $post['codigo'];
				}
				if(($codigo == 0) or ($codigo == 3)){
					log_message('debug', '...codigo de exito');
					
					$this->load->model('md_pages');

                                        //ndp 20150728 - esta es la modificacion

					$payment = $this->md_pages->get_info_of_payment_by_folio($post['referencia']);
					log_message('debug', '$payment='.print_r($payment, TRUE));		
					if($payment){
						$this->load->library('lb_functions');
						$this->md_invoicing->update_payment($data, $payment->id_pago);
						$this->lb_functions->create_file_for_sap($payment);
						$exists = true;

                                                $desplegarPantallaExito = 'si';

                                                $info_pago = $this->md_invoicing->get_informacion_pago($payment->id_pago);
                                                $carrito = json_decode($info_pago['informacion'],TRUE);
					}
				}
				else{
					//error en el pago
				}
			}
		}
		if(!$exists){
			$this->md_invoicing->save_no_payment($data);
		}

        


		$send = array(
			'carousel_config' => $this->carousel->thanks(),
                        'post' => $post,
                        'get' => $get,
                        'desplegarPantallaExito' => $desplegarPantallaExito,
                        'carrito'=> $carrito
		);


                //array_push($send, $post);

		load_view('thanks', $send);
	}
}