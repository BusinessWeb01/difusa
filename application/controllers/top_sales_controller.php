<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Top_sales_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->model('top_sales_model');
			$this->load->library('lb_send_mail');
	}

	function getTopSales()
	{
		$codigos = $this->top_sales_model->getIdProduct();
		$mostrar = 0;
		$topSalesArray = array();
		$topSalesTemp = array();
		$codProd = array();
		$send = array();
		$productosTop = array();
		foreach ($codigos as $key)
		{
			$codProd[$key['id_product']] = $this->top_sales_model->getSalesCount($key['id_product']);
		}
		arsort($codProd);
		foreach($codProd as $codigo)
		{
			$indices = (array_keys($codProd));
			$productosTop[$indices[$mostrar]] = $this->top_sales_model->getDataPerCode($indices[$mostrar]);
			$topSalesTemp[$mostrar] = array('id_product' => $indices[$mostrar]);
			$mostrar++;
			if($mostrar == 9) break;
		}
		$send = array('codigos' => $productosTop);
		$this->top_sales_model->fillCacheSales($topSalesTemp);
		$this->load->view('top_sales',$send);
	}
}