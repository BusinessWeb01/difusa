<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categoria extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->library('carousel');
			$this->load->model('md_category');
	}

	function insumos()
	{
		log_message('debug', 'md_category->categoria($category)');

			$category = 'INS';
			$send = array(
				'carousel_config' => $this->carousel->categorias(),
				'info'=>$this->md_category->categoria($category)
			);
 
				$this->load->view('categorias',$send);
		
		log_message('debug', 'md_category->categoria(INS).$this->db = '.print_r($this->db,TRUE));
	}

	function complementos()
	{
		log_message('debug', 'md_category->categoria($category)');

			$category = 'COM';
			$send = array(
				'carousel_config' => $this->carousel->categorias(),
				'info'=>$this->md_category->categoria($category)
			);
 
				$this->load->view('categorias',$send);
		
		log_message('debug', 'md_category->categoria(INS).$this->db = '.print_r($this->db,TRUE));
	}

}

?>