<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacto_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->model('contact_model');
			$this->load->library('lb_send_mail');
			$this->load->library('carousel');
	}
	function send($option)
	{
		if(strcmp($option,'Contacto')==0)
		{
			load_view('contact');
		}
		elseif(strcmp($option,'Equipo')==0)
		{
			load_view('team_contact');
		}
	}
	function getDataComment()
	{
		sleep(5);
		log_message('debug','Entro a getDataComment');
		$nombre = $this->input->post('name',TRUE);
		$telefono = $this->input->post('phone',TRUE);
		$correo = $this->input->post('email',TRUE);
		$asunto = $this->input->post('asunto',TRUE);
		$mensaje = $this->input->post('comment',TRUE);
		$tipo = $this->input->post('tipo',TRUE);
		$sendDataContact = array(
			'name' => $nombre,
			'phone' => $telefono,
			'email' => $correo,
			'asunto' => $asunto,
			'comment' => $mensaje,
			'tipo' => $tipo
			);
		$bandera = $this->contact_model->save($sendDataContact);
		log_message('debug','$bandera='.$bandera);
		if(!$bandera)
		{
			$send = array('error'=> 'error');
			$this->load->view('contact',$send);
		}
		else
		{
			$this->lb_send_mail->send_mail($sendDataContact);
			$data_send = array(
				'data' => $sendDataContact
			);
			//$this->load->view('email_contact_template', $data_send);
			$send = array('success' => 'ok',
					'carousel_config' => $this->carousel->categorias()
				);			
			load_view('home',$send);
		}
	}
}
?>