<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promociones extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_promocion');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
	}
	
	function productos()
	{
		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_promocion->productos(),
				'pais'=>$this->md_promocion->pais(),
				'marca'=>$this->md_promocion->marca(),
				'cate'=>$this->md_promocion->catego(),
				'presentacion'=>$this->md_promocion->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0
			);
 				
				$this->load->view('promociones',$send);
	}
	function articulos()
	{
		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_promocion->productos(),
				'pais'=>$this->md_promocion->pais(),
				'marca'=>$this->md_promocion->marca(),
				'cate'=>$this->md_promocion->catego(),
				'presentacion'=>$this->md_promocion->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0
			);
 				
				$this->load->view('promociones',$send);
		}

	function paginas()
	{
		$ini=$_GET['ini'];
		$numero=$_GET['numero'];
		$cant=$_GET['cant'];
		$prod=$_GET['cat'];

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_promocion->productos($prod),
				'pais'=>$this->md_promocion->pais(),
				'marca'=>$this->md_promocion->marca(),
				'cate'=>$this->md_promocion->catego(),
				'presentacion'=>$this->md_promocion->presentacion(),
				'cant'=>$cant,
				 'numero'=>$numero,
				 'ini'=>$ini,
				 
			);

			
 			
				$this->load->view('promociones',$send);
	}

	function filtro()
	{

		if ((!isset($_POST['marca']))&&(!isset($_POST['presentacion']))&&(!isset($_POST['pais']))&&(!isset($_POST['cate']))) {
			$marca="%";
			$presen="%";
			$pais="%";
			$prod="%";
		}
		else{
		$marca=$_POST['marca'];
		$presen=$_POST['presentacion'];
		$pais=$_POST['pais'];
		$prod=$_POST['cate'];
		}
		if (isset($_POST['mostrar'])) {
			$canti=(int)$_POST['mostrar'];
		}
		else{
			$canti=8;
		}
		if ($canti<1){
			$canti=8;
		}

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_promocion->filtro($marca,$presen,$pais,$prod),
				'pais'=>$this->md_promocion->pais(),
				'cate'=>$this->md_promocion->catego(),
				'marca'=>$this->md_promocion->marca(),
				'presentacion'=>$this->md_promocion->presentacion(),
				'cant'=>$canti,
				 'numero'=>$canti,
				 'ini'=>0
			);

			
 				
				$this->load->view('promociones',$send);


	}


	function agregarProducto()
    {
    	
        $id = $_POST['idproduct'];
        $producto = $this->md_promocion->info($id);
        $cantidad = $_POST['cantidad'];
        $nombre = $_POST['nombre'];
        $precio2 = $_POST['precio'];
        $codigo = $_POST['codigo'];
        $moneda_origen = $_POST['moneda'];

        $moneda_destino='MXN';
		$get = file_get_contents("https://www.google.com/finance/converter?a=$precio2&from=$moneda_origen&to=$moneda_destino");
		$get = explode("<span class=bld>",$get);
		$get = explode("</span>",$get[1]);

     	$precio=$precio2;
     	//$precio=preg_replace("/[^0-9\.]/", null, $get[0]);
        //obtenemos el contenido del carrito
        $carrito = $this->cart->contents();
 
        foreach ($carrito as $item) {
            //si el id del producto es igual que uno que ya tengamos
            //en la cesta le sumamos uno a la cantidad
            if ($item['id'] == $id) {
                $cantidad = $cantidad + $item['qty'];
            }
        }
        //cogemos los productos en un array para insertarlos en el carrito
        $insert = array(
            'id' => $id,
            'qty' => $cantidad,
            'price' => $precio,
            'name' => $nombre
        );
        //si hay opciones creamos un array con las opciones y lo metemos
        //en el carrito
        if ($codigo) {
            $insert['options'] = array(
            'code'=> $codigo
            );
        }
        //insertamos al carrito
        $this->cart->insert($insert);
        
        //redirigimos mostrando un mensaje con las sesiones flashdata
        //de codeigniter confirmando que hemos agregado el producto
        $this->session->set_flashdata('agregado', 'El producto fue agregado correctamente');
     
		$prod = 'COA';
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_promocion->productos($prod),
				'pais'=>$this->md_promocion->pais(),
				'marca'=>$this->md_promocion->marca(),
				'cate'=>$this->md_promocion->catego(),
				'presentacion'=>$this->md_promocion->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0
			);
 				
				$this->load->view('promociones',$send);
			}

}


