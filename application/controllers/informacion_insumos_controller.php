<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informacion_insumos extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_informacion');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
	}

	function producto($id)
	{
		$user = $this->session->userdata('id_client');
		$verifica = $this->md_informacion->getWish($user,$id);
		if($verifica)
		{
			$send = array(
				'carousel_config' => $this->carousel->informacion(),
				'info' => $this->md_informacion->prod($id),
				'wish' => 'ok'
			);
			$this->load->view('informacion_insumos',$send);
		}
		else
		{
			$send = array(
				'carousel_config' => $this->carousel->informacion(),
				'info'=>$this->md_informacion->prod($id)
			);
			$this->load->view('informacion_insumos',$send);
		}
	}

}
?>