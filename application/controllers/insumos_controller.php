<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Insumos_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_insumos');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
   		$this->load->library('lb_moneda');
	}
	function productos()
	{
		log_message('debug', 'md_insumos->producto($prod)');
			$prod = 'MAL';
			$inicio= 0;
			$fin= 8;
			$infoGral = $this->md_insumos->prod($prod,$inicio,$fin); //meto los productos en infoGral, para ir realizando la conversion de los precios y los vuelvo a guardar en los indices correspondientes
			for($c=0;$c<count($infoGral);$c++)
			{
				$cantidad = $infoGral[$c]['price'];
				$divisaOrigen = $infoGral[$c]['currency_name'];
				$divisaDestino = $this->session->userdata('destino_cambio');
				if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
				{
					$nuevaCantidad = $this->lb_moneda->conversor_monedas($divisaOrigen,$divisaDestino,$cantidad);
					$infoGral[$c]['price'] = $nuevaCantidad;
					$infoGral[$c]['currency_name'] = $divisaDestino;
				}
			}
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->productos($prod),
				'info2'=>$infoGral,
				'pais'=>$this->md_insumos->pais(),
				'marca'=>$this->md_insumos->marca(),
				'cate'=>$this->md_insumos->catego(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>8,
				'numero'=>8,
				'ini'=>0,
				'pagina'=>1,
				'marca1'=>0,
				'presen1'=>0,
				'cat1'=>0,
				'pais1'=>0
			);
				$this->load->view('insumos',$send);
		
		log_message('debug', 'md_insumos->productos(INS).$this->db = '.print_r($this->db,TRUE));
	}
	

	function filtro($ini,$numero,$cant,$prod,$pais1,$presen1,$marca1,$pagina)
	{
		
		if ((isset($_POST['marca']))&&(isset($_POST['presentacion']))&&(isset($_POST['cate']))) {
		$marca=$_POST['marca'];
		$presen=$_POST['presentacion'];
		$pais=$_POST['pais'];
		$prod=$_POST['cate'];
		}
		if (isset($_POST['mostrar'])) {
			$canti=(int)$_POST['mostrar'];
		}
		else{
			$canti=$cant;
		}
		if ($canti<1){
			$canti=8;
		}

		if (!isset($_POST['pais'])) {	
			if (isset($pais1)) {
				if (strcmp($pais1, "0") === 0) {
   				$pais="%";
				}
				else{
				$pais=$pais1;
				}

			}
		}

		if (!isset($_POST['presentacion'])) {	
			if (isset($presen1)) {
				if (strcmp($presen1, "0") === 0) {
   				$presen="%";
				}
				else{
				$presen=$presen1;
				}

			}
		}

		if (!isset($_POST['marca'])) {	
			if (isset($marca1)) {
				if (strcmp($marca1, "0") === 0) {
   				$marca="%";
				}
				else{
				$marca=$marca1;
				}

			}
		}

		$infoGral = $this->md_insumos->filtro2($marca,$presen,$pais,$prod,$ini,$canti);
		for($c=0;$c<count($infoGral);$c++)
		{
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				$nuevaCantidad = $this->lb_moneda->conversor_monedas($divisaOrigen,$divisaDestino,$cantidad);
				$infoGral[$c]['price'] = $nuevaCantidad;
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
		}
		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->filtro($marca,$presen,$pais,$prod),
				'info2'=>$infoGral,
				'pais'=>$this->md_insumos->pais(),
				'cate'=>$this->md_insumos->catego(),
				'marca'=>$this->md_insumos->marca(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>$canti,
				 'numero'=>$canti,
				 'ini'=>0,
				 'pagina'=>$pagina,
				 'pais2'=>$this->md_insumos->pais1($pais),
				'marca2'=>$this->md_insumos->marca1($marca),
				'presen2'=>$this->md_insumos->presentacion1($presen),
				'cat2'=>$this->md_insumos->catego1($prod),
				'marca1'=>$marca,
				'presen1'=>$presen,
				'cat1'=>$prod,
				'pais1'=>$pais
			);
		$this->load->view('insumos',$send);
	}
}
