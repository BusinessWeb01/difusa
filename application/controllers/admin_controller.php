<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_controller extends CI_Controller 
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->library('security');
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
        $this->load->model('admin_model');
    }

    function allClients()
    {
    	$crud = new grocery_CRUD();
        
    	$crud->set_table('clients');
        $crud->set_subject('Clientes');
        $crud->unset_add();
        $crud->unset_print();
        $crud->unset_export();
        $crud->unset_edit();
        $crud->columns('client_name','last_name_client','last_name2_client','contact_mail','register_date');
        $crud->display_as('client_name','Nombre del cliente');
        $crud->display_as('last_name_client','Apellido Paterno');
        $crud->display_as('last_name2_client','Apellido Materno');
        $crud->display_as('contact_mail','Correo');
        $crud->display_as('register_date','Fecha de Registro');

    	$output = $crud->render();

    	$this->usersView($output);
    }

    function orders()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('orders');
        $crud->set_subject('Ordenes');
        /*--------------------------nombres de usuarios------------------------------*/
        $names = $this->admin_model->getUsersName();
        $arrayNames = array();
        for($c=0;$c<count($names);$c++)
            $arrayNames[$names[$c]['id_client']] = $names[$c]['client_name']." ".$names[$c]['last_name_client'];
        foreach ($arrayNames as $key => $value) 
        {
            $crud->field_type($key,'dropdown',$value);
        }
        $drops = array(
                'id_user' => $arrayNames
            );
        foreach ($drops as $key => $value) 
        {
            $crud->field_type($key,'dropdown',$value);
        }   
        $fields = array(
                'order_status' => 'Status',
                'order_date' => 'Fecha de orden',
                'id_user' => 'Usuario',
                'order_total' => 'Total'
            );
        foreach ($fields as $key => $value) 
        {
            $crud->display_as($key,$value);
        }
        $crud->edit_fields('order_status');
        //$crud->edit_fields('id_user');
        $crud->unset_add();
        $crud->unset_print();
        $crud->unset_export();
        $output = $crud->render();

        $this->usersView($output);
    }
    
    function products()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('products');
        $crud->set_subject('Producto');
        $crud->unset_print();
        $crud->unset_export();
        $crud->set_primary_key('id_product','products');
        $crud->columns('product_name','product_description','price','id_currency');
        $crud->required_fields('id_sap','product_code','product_name','id_category','id_sub_category','contry_name_iso3','id_product_type','id_brand','id_presentation','price','id_currency');
        /*-----------------------Obtener subcategorias-----------------------*/
        $subCategory = $this->admin_model->getSubCategory();
        $arraySubCategory = array();
        for($c=0;$c<count($subCategory);$c++) 
            $arraySubCategory[$subCategory[$c]['id_sub_category']] = $subCategory[$c]['sub_category'];
        /*------------------------------Obtener paises-----------------------*/
        $countries = $this->admin_model->getCountry();
        $arrayCountries = array();
        for($c=0;$c<count($countries);$c++) 
            $arrayCountries[$countries[$c]['country_name_iso3']] = $countries[$c]['country_name'];
        /*-------------------------Obtener tipo de producto-----------------*/
        $productType = $this->admin_model->getProductType();
        $arrayProductType = array();
        for($c=0;$c<count($productType);$c++)
            $arrayProductType[$productType[$c]['id_product_type']] = $productType[$c]['product_type'];
        /*---------------------------Marcas a mostrar-----------------------*/
        $brands = $this->admin_model->getBrands();
        $arrayBrands = array();
        for($c=0;$c<count($brands);$c++)
            $arrayBrands[$brands[$c]['id_brand']] = $brands[$c]['brand'];
        /*-----------------presentaciones----------------------------------*/
        $presentation = $this->admin_model->getPresentation();
        $arrayPresentation = array();
        for($c=0;$c<count($presentation);$c++)
            $arrayPresentation[$presentation[$c]['id_presentation']] = $presentation[$c]['presentation'];
        /*-------------------------Campos a mostrar------------------------*/
        $fields = array(
                'id_sap' => 'Sap',
                'product_code' => 'Código de producto',
                'id_category' => 'Categoría',
                'id_sub_category' => 'Subcategoría',
                'product_name' => 'Nombre del producto',
                'product_description' => 'Descripción',
                'country_name_iso3' => 'Pais',
                'id_product_type' => 'Tipo del producto',
                'id_brand' => 'Marca',
                'id_presentation' => 'Presentación',
                'price' => 'Precio',
                'iva_tax' => 'IVA',
                'id_currency' => 'Moneda',
                'promotion' => 'Promoción',
                'before_price' => 'Precio Anterior',
                'height' => 'Altura',
                'width' => 'Anchura',
                'long' => 'Longitud',
                'weight' => 'Peso',
                'url_images' => 'Ruta de imagen',
                'url_techsheets' => 'Ruta de ficha tecnica',
                'slug' => 'URL'
            );
        foreach ($fields as $key => $value) 
        {
            $crud->display_as($key,$value);
        }
        /*------------------------menus desplegables-------------------------*/
        $drops = array(
                'id_category' => array('INS' => 'Insumos', 'COM' => 'Complementos'),
                'id_sub_category' => $arraySubCategory,
                'country_name_iso3' => $arrayCountries,
                'id_currency' => array('1' => 'Pesos Mexicanos', '2' => 'Dólar Americano','3' => 'Euro'),
                'id_product_type' => $arrayProductType,
                'id_brand' => $arrayBrands,
                'id_presentation' => $arrayPresentation
            );
        foreach ($drops as $key => $value) 
        {
            $crud->field_type($key,'dropdown',$value);
        }
        $crud->unset_add_fields('created_at');
        $output = $crud->render();

        $this->usersView($output);
    }

    function allUsers()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('users');
        $crud->set_subject('usuarios');
        $crud->set_primary_key('id');
        $crud->unset_print();
        $crud->unset_export();
        $crud->field_type('password','password');
        $crud->columns('username','email','created');
        $crud->display_as('username','Usuario');
        $crud->display_as('email','Correo');
        $crud->display_as('created','Fecha de alta');
        $output = $crud->render();

        $this->usersView($output);
    }

    function categories()
    {
        $crud = new grocery_CRUD();
        $crud->set_table('categories');
        $crud->set_subject('Categorias');
        $crud->set_primary_key('id_category');
        $crud->unset_print();
        $crud->unset_export();
        $crud->columns('category','slug');
        $columnas = array(
                'id_category' => 'Id Categoria',
                'category' => 'Categoria',
                'skug' => 'URL'
            );
        foreach ($columnas as $key => $value)
            $crud->display_as($key,$value);
        $crud->unset_add_fields('created_at');

        $output = $crud->render();

        $this->usersView($output);   
    }
    function usersView($output = null)
    {
    	$this->load->view('admin_view.php',$output);
    }
}