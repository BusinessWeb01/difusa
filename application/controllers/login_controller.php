<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->load->model('login_model');
			$this->load->helper('security');
			$this->load->library('carousel');
	}
	function index()
	{	
		$this->load->view('login_view');
	}
	function loguea()
	{		
		$username = $this->input->post('username',TRUE);
		$password = $this->input->post('password',TRUE);
		$admin = $this->login_model->admin($username,$password);
		if($admin)
		{
			$send = array(
				'carousel_config' => $this->carousel->categorias()
			);
			$this->session->set_userdata($admin[0]);
			load_view('home',$send);
		}
		$password = do_hash($password,'md5');
		$send2 = $this->login_model->logueo($username,$password);
		if($send2 && !$admin)
		{
			$send = array(
				'carousel_config' => $this->carousel->categorias()
			);
			$this->session->set_userdata($send2[0]);
			load_view('home',$send);
		}
		else
		{
			$send = array(
				'error' => 'error'
			);
			$this->load->view('login_view', $send);
		}
	}
	function desloguea()
	{		
		$this->session->unset_userdata('session_id');
		redirect(base_url());
	}
	function regresa()
	{		
		redirect(base_url());
	}
}
?>