<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articulos_insumos_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_insumos');
   		$this->load->helper('text');
   		$this->load->library('Lb_carro');
   		$this->load->library(array('pagination', 'cart'));
   		$this->load->library('lb_moneda');
	}

	function productos()
	{
		log_message('debug', 'md_insumos->producto($prod)');
			$prod = 'MAL';
			$inicio= 0;
			$fin= 8;
			$infoGral = $this->md_insumos->prod($prod,$inicio,$fin);
			for($c=0;$c<count($infoGral);$c++)
			{
				$cantidad = $infoGral[$c]['price'];
				$divisaOrigen = $infoGral[$c]['currency_name'];
				$divisaDestino = $this->session->userdata('destino_cambio');
				if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
				{
					$nuevaCantidad = $this->lb_moneda->conversor_monedas($divisaOrigen,$divisaDestino,$cantidad);
					$infoGral[$c]['price'] = $nuevaCantidad;
					$infoGral[$c]['currency_name'] = $divisaDestino;
				}
			}
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->productos($prod),
				'info2'=>$infoGral,
				'pais'=>$this->md_insumos->pais(),
				'marca'=>$this->md_insumos->marca(),
				'cate'=>$this->md_insumos->catego(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0,
				 'pagina'=>1,
				'marca1'=>0,
				'presen1'=>0,
				'cat1'=>0,
				'pais1'=>0
			);
				$this->load->view('insumos_user',$send);
		
		log_message('debug', 'md_insumos->productos(INS).$this->db = '.print_r($this->db,TRUE));
	}
	

	function filtro($ini,$numero,$cant,$prod,$pais1,$presen1,$marca1,$pagina)
	{
		
		if ((isset($_POST['marca']))&&(isset($_POST['presentacion']))&&(isset($_POST['cate']))) {
		$marca=$_POST['marca'];
		$presen=$_POST['presentacion'];
		$pais=$_POST['pais'];
		$prod=$_POST['cate'];
		}
		if (isset($_POST['mostrar'])) {
			$canti=(int)$_POST['mostrar'];
		}
		else{
			$canti=$cant;
		}
		if ($canti<1){
			$canti=8;
		}

		if (!isset($_POST['pais'])) {	
			if (isset($pais1)) {
				if (strcmp($pais1, "0") === 0) {
   				$pais="%";
				}
				else{
				$pais=$pais1;
				}

			}
		}

		if (!isset($_POST['presentacion'])) {	
			if (isset($presen1)) {
				if (strcmp($presen1, "0") === 0) {
   				$presen="%";
				}
				else{
				$presen=$presen1;
				}

			}
		}

		if (!isset($_POST['marca'])) {	
			if (isset($marca1)) {
				if (strcmp($marca1, "0") === 0) {
   				$marca="%";
				}
				else{
				$marca=$marca1;
				}

			}
		}

		$infoGral = $this->md_insumos->filtro2($marca,$presen,$pais,$prod,$ini,$canti);
		for($c=0;$c<count($infoGral);$c++)
		{
			$cantidad = $infoGral[$c]['price'];
			$divisaOrigen = $infoGral[$c]['currency_name'];
			$divisaDestino = $this->session->userdata('destino_cambio');
			if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
			{
				$nuevaCantidad = $this->lb_moneda->conversor_monedas($divisaOrigen,$divisaDestino,$cantidad);
				$infoGral[$c]['price'] = $nuevaCantidad;
				$infoGral[$c]['currency_name'] = $divisaDestino;
			}
		}

		$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->filtro($marca,$presen,$pais,$prod),
				'info2'=>$infoGral,
				'pais'=>$this->md_insumos->pais(),
				'cate'=>$this->md_insumos->catego(),
				'marca'=>$this->md_insumos->marca(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>$canti,
				 'numero'=>$canti,
				 'ini'=>0,
				 'pagina'=>$pagina,
				 'pais2'=>$this->md_insumos->pais1($pais),
				'marca2'=>$this->md_insumos->marca1($marca),
				'presen2'=>$this->md_insumos->presentacion1($presen),
				'cat2'=>$this->md_insumos->catego1($prod),
				'marca1'=>$marca,
				'presen1'=>$presen,
				'cat1'=>$prod,
				'pais1'=>$pais
			);

			
 
				$this->load->view('insumos_user',$send);


	}
	
	function agregarProducto()
    {
        $id = $_POST['idproduct'];
        $producto = $this->md_insumos->info($id);
        $cantidad = $_POST['cantidad'];
        $nombre = $_POST['nombre'];
        $precio = $_POST['precio'];
        $codigo = $_POST['codigo'];
        $moneda_origen = $_POST['moneda'];
		$moneda_destino=$this->session->userdata('destino_cambio');

		$this->lb_carro->agregar($id,$cantidad,$precio,$nombre,$codigo);
        /*
		$get = file_get_contents("https://www.google.com/finance/converter?a=$precio2&from=$moneda_origen&to=$moneda_destino");
		$get = explode("<span class=bld>",$get);
		$get = explode("</span>",$get[1]);*/

     	
     	//$precio=preg_replace("/[^0-9\.]/", null, $get[0]);
        //obtenemos el contenido del carrito
       
        
        //redirigimos mostrando un mensaje con las sesiones flashdata
        //de codeigniter confirmando que hemos agregado el producto
        $this->session->set_flashdata('agregado', 'El producto fue agregado correctamente');
     
     
		$prod = 'MAL';
			$inicio= 0;
			$fin= 8;
			$send = array(
				'carousel_config' => $this->carousel->productos(),
				'info'=>$this->md_insumos->productos($prod),
				'info2'=>$this->md_insumos->prod($prod,$inicio,$fin),
				'pais'=>$this->md_insumos->pais(),
				'marca'=>$this->md_insumos->marca(),
				'cate'=>$this->md_insumos->catego(),
				'presentacion'=>$this->md_insumos->presentacion(),
				'cant'=>8,
				 'numero'=>8,
				 'ini'=>0,
				 'pagina'=>1,
				'marca1'=>0,
				'presen1'=>0,
				'cat1'=>0,
				'pais1'=>0
			);
 			
 				
				$this->load->view('insumos_user',$send);

    }
    
    
    function eliminarCarrito() {
        $this->lb_carro->eliminarCarro();
        $this->load->view('carro');
    }

    function carrito()
    {
    	
		$this->load->view('carro');
    }

    function insertWish()
    {
    	$producto = $this->input->post('idproduct',TRUE);
    	$user = $this->session->userdata('id_client');
    	$wishInsert = array(
    		'id_user' => $user,
    		'id_product' => $this->input->post('idproduct',TRUE)
    	);
    	$this->md_insumos->insertWish($wishInsert);
    	echo "<script language='javascript'>alert('Producto agregado correctamente');</script>";
    	$prod = 'MAL';
		$inicio= 0;
		$fin= 8;
		$send = array(
			'carousel_config' => $this->carousel->productos(),
			'info'=>$this->md_insumos->productos($prod),
			'info2'=>$this->md_insumos->prod($prod,$inicio,$fin),
			'pais'=>$this->md_insumos->pais(),
			'marca'=>$this->md_insumos->marca(),
			'cate'=>$this->md_insumos->catego(),
			'presentacion'=>$this->md_insumos->presentacion(),
			'cant'=>8,
	    	'numero'=>8,
		    'ini	'=>0,
	    	'pagina' => 1,
			'marca1'=>0,
			'presen1'=>0,
			'cat1'=>0,
			'pais1'=>0
		);
 		$this->load->view('insumos_user',$send);
    }
}

?>