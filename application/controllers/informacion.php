<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Informacion extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('carousel');
		$this->load->model('md_informacion');
		$this->load->library(array('pagination', 'cart'));
   		$this->load->helper('text');
   		$this->load->library('lb_moneda');
	}

	function producto($id)
	{
		$infoGral = $this->md_informacion->prod($id);
		$cantidad = $infoGral[0]['price'];
		$divisaOrigen = $infoGral[0]['currency_name'];
		$divisaDestino = $this->session->userdata('destino_cambio');
		if((strcmp($divisaDestino, $divisaOrigen)) !== 0)
		{
			$nuevaCantidad = $this->lb_moneda->conversor_monedas($divisaOrigen,$divisaDestino,$cantidad);
			$infoGral[0]['price'] = $nuevaCantidad;
			$infoGral[0]['currency_name'] = $divisaDestino;
		}
		
		$send = array(
			'carousel_config' => $this->carousel->informacion(),
			'info'=>$infoGral
		);
		$this->load->view('informacion',$send);
	}

}

?>