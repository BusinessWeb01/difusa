<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {
	 
	 function __construct(){
		  parent::__construct();
		  $this->load->helper('url');
		  $this->load->model('md_products');
		  $this->load->library('lb_products');
		  $this->load->helper('form');
		  $this->load->library('form_validation');
		  $this->load->library('security');
		  $this->load->library('tank_auth');
		  $this->lang->load('tank_auth');
		  $this->load->model('md_pages');
		  /*$admin = $this->md_products->user_is_admin($this->tank_auth->get_user_id());
		  if(!$admin){
			   $this->tank_auth->logout();
			   redirect('/auth/login/');
		  }*/
		  if (!$this->tank_auth->is_logged_in()) {
			   redirect('/auth/login/');
		  }
	 }
	 function delete_img($product_meta_id = 0, $load_in = 'imagenes'){
		  $product_meta_id = intval($product_meta_id);
		  $this->db->select('
			   id_product_meta,
			   id_product,
			   meta_value
		  ');
		  $this->db->where('id_product_meta', $product_meta_id);
		  $this->db->from('products_meta');
		  $d = false;
		  $q = $this->db->get();
		  if ($q->num_rows > 0){
			   foreach ($q->result() as $row){
				    $d['product_id'] =  $row->id_product;
				    $d['meta_value'] =  $row->meta_value;
			   }
		  }
		  if($d){
			   $this->db->delete('products_meta', array('id_product_meta' => $product_meta_id));
			   $path_img = 'content/uploads/images/products/'.$d['meta_value'];
			   if (file_exists($path_img)){
				    unlink($path_img);
			   }
			   $redirect = 'products/edit/'.$d['product_id'].'#'.$load_in;
			   redirect($redirect);
		  }
		  else{
			   redirect('admin/products');
		  }
	 }
	 
	 function get_posts_product(){
		  
		  foreach($this->lb_products->get_product_fields() as $key => $value){
			   $product_data[$key] = $this->input->post($key);
		  }
		  
		  $data_igm = true;
		  foreach ($_FILES['img_product']['name'] as $file){
			   $ext_img = strtolower(pathinfo($file, PATHINFO_EXTENSION));
			   if(!($ext_img == 'jpg' or $ext_img == 'png' or $ext_img=='gif')){
				    $data_igm = false;
			   }
		  }
		  
		  
		  $data_igm_upload = false;
		  $this->load->library('upload');
		  if ($data_igm){
			   $config['upload_path'] = './content/uploads/images/products/';
			   $config['allowed_types'] = 'jpg|png|jpeg|gif';
			   $config['max_size'] = '0';
			   $config['max_width']  = '0';
			   $config['max_height']  = '0';
			   $this->upload->initialize($config);
			   $this->upload->do_multi_upload('img_product');
			   $data_igm_upload = $this->upload->get_multi_upload_data();
		  }
		  $send = array(
			   'data_product' => $product_data,
			   'categories_rings' => $this->input->post('categories_rings'),
			   'category_collection' => $this->input->post('category_collection'),
			   'img_product' => $data_igm_upload
		  );
		  return $this->lb_products->valid_required_fields($send);
	 }
	 
	 function edit($id = 0){
		  $error = false;
		  $success = false;
		  if($this->input->post('edit_product')){
			   $id = $this->input->post('edit_product');
			   $data = $this->get_posts_product();
			   if(!$data){
				    $error = 'required';
			   }
			   else{
				    $update_info = $this->md_products->update_product($data, $id);
				    if($update_info){
					     $success = true;
				    }
			   }
		  }
		  $send = array(
			   'title_form' => 'Editar producto',
			   'edit_product' => true,
			   'product_error' => $error,
			   'product_success' => $success,
			   'measures' => $this->md_pages->get_attributes(),
			   'colors' => $this->md_pages->get_attributes('id_rings_color', 'color', 'rings_colors'),
			   'points' => $this->md_pages->get_attributes('id_rings_point', 'point', 'rings_points'),
			   'kilates' => $this->md_pages->get_attributes('id_rings_kilate', 'kilate', 'rings_kilates'),
			   'categories_rings' => $this->md_pages->get_categories_rings(),
			   'categories_collections' => $this->md_pages->get_categories_collections(),
			   'info_product' => $this->md_products->get_all_product_info_by_id($id)
		  );
		  //pre($send['info_product']);
		  $this->load->view('admin/simple.php',$send);
	 }
	 
	 function add(){
		  $error = false;
		  $success = false;

                  //ndp 20150718 0253 - IK
                  $post = $this->input->post();
                  log_message('debug', 'products.php.add().$post = '.print_r($post,TRUE));

		  if($this->input->post('add_product')){
			   $data = $this->get_posts_product();
			   if(!$data){
				    $error = 'required';
			   }
			   else{
				    $insert_info = $this->md_products->save_product($data);
				    if($insert_info){
					     $success = true;
				    }
			   }
		  }

                  //ndp 20150717 - llena catalogos
		  $send = array(
			   'title_form' => 'Agregar producto',
			   'add_product' => true,
			   'product_error' => $error,
			   'product_success' => $success,
			   'measures' => $this->md_pages->get_attributes(),
			   'colors' => $this->md_pages->get_attributes('id_rings_color', 'color', 'rings_colors'),
			   'points' => $this->md_pages->get_attributes('id_rings_point', 'point', 'rings_points'),
			   'kilates' => $this->md_pages->get_attributes('id_rings_kilate', 'kilate', 'rings_kilates'),
			   'categories_rings' => $this->md_pages->get_categories_rings(),
			   'categories_collections' => $this->md_pages->get_categories_collections()
		  );
		  //pre($send['categories_collections']);
		  $this->load->view('admin/simple.php',$send);
	 }
	 
}