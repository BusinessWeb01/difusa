<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registra_controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->helper('form');
			$this->load->helper('security');
			$this->load->library('form_validation');
			$this->load->model('registra_model');
			$this->load->library('carousel');
			$this->load->model('panel_model');
	}
	function index()
	{
		$news = $this->panel_model->getNews();
		$send = array('news' => $news);
		$this->load->view('registra_view',$send);
	}
	function registrar()
	{
		//datos del cliente
		$nuevoCorreo = $this->input->post('usuario',TRUE);
		$correo = $this->registra_model->getMails();
		$bandera = true;
		for($c = 0 ; $c < count($correo) ; $c++)
		{			
			if(strcmp($correo[$c]['mails'],$nuevoCorreo) == 0)
			{
				$send = array(
					'carousel_config' => $this->carousel->home(),
					'section_nav' => 'home',
					'error2' => 'correo',
					'news' => ($this->panel_model->getNews())
				);
				$bandera = false;
				$this->load->view('registra_view',$send);
				break;
			}			
		}
		if($bandera == true)
		{
			$this->registrarOk();
		}
	}
	function registrarOk()
	{
		$password = $this->input->post('password',TRUE);
		$password2 = $this->input->post('password2',TRUE);		
		if(strcmp($password,$password2) == 0)
		{
			$password = do_hash($password,'md5');
			$data = array(
				"client_name" => $this->input->post('client_name',TRUE),
				"last_name_client" => $this->input->post('last_name_client',TRUE),
				"last_name2_client" => $this->input->post('last_name2_client',TRUE),
				"state" => $this->input->post('country',TRUE),
				"delegation" => $this->input->post('delegation',TRUE),
				"suburb" => $this->input->post('suburb',TRUE),
				"locality" => $this->input->post('locality',TRUE),
				"street" => $this->input->post('street',TRUE),
				"outdoor_number" => $this->input->post('outdoor',TRUE),
				"indoor_number" => $this->input->post('indoor',TRUE),
				"zipcode" => $this->input->post('zipcode',TRUE),
				"contact_mail" => $this->input->post('usuario',TRUE),
				"phone_number" => $this->input->post('local',TRUE),
				"cellphone_number" => $this->input->post('celular',TRUE),
				"user_name" => $this->input->post('usuario',TRUE),
				"password" => $password
			);
			print_r($data);
			$this->registra_model->registro($data);
			$data2 = array(
				"street_bussiness" => $this->input->post('street2',TRUE),
				"outdoor_number_bussiness" => $this->input->post('outdoor2',TRUE),
				"indoor_number_bussiness" => $this->input->post('indoor2',TRUE),
				"suburb_bussiness" => $this->input->post('suburb2',TRUE),
				"locality_bussiness" => $this->input->post('locality2',TRUE),
				"delegation_bussiness" => $this->input->post('delegation2',TRUE),
				"state_bussiness" => $this->input->post('country2',TRUE),
				"zipcode_bussiness" => $this->input->post('zipcode2',TRUE),
				"rfc" => $this->input->post('rfc',TRUE),
				"email_bussiness" => $this->input->post('email',TRUE),
				"phone_number_bussiness" => $this->input->post('local2',TRUE),
				"cellphone_number_bussiness" => $this->input->post('celular2',TRUE)
				);
			print_r($data2);
			$this->registra_model->registroFactura($data2);
			if($this->input->post('suscribir') != NULL )
			{
				$suscribe = $this->input->post('newsuser',TRUE);				
				$mandaSuscripcion = array();				
				for($c=0;$c<count($suscribe);$c++)
				{
					$mandaSuscripcion[$c] = array('id_user' => $this->registra_model->newUserSuscription(),
													'id_subscription' => $suscribe[$c]
						);
				}
				$this->registra_model->insertNewSuscription($mandaSuscripcion);
			}
			$send = array(
				'carousel_config' => $this->carousel->home(),
				'section_nav' => 'home'
			);
			echo "<script language='javascript'>alert('Registro Exitoso');</script>";
			load_view('home', $send);
		}
		else
		{
			$send = array(
					'carousel_config' => $this->carousel->home(),
					'section_nav' => 'home',
					'error' => 'contraseñas',
					'news' => ($this->panel_model->getNews())
				);
			$this->load->view('registra_view',$send);
		}
	}
}
?>