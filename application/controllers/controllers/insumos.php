<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Insumos extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->library('carousel');
	}

	function index()
	{
			$send = array(
			'carousel_config' => $this->carousel->home(),
			'section_nav' => 'home'
		);
		$this->load->view('categorias');
	}
}

?>