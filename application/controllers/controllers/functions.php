<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Clase creada para controlar las funciones del carrito de compra
*/
class Functions extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('lb_functions');
	}	
	
	function index(){
		redirect(base_url());
	}
	
	function change_current_currency($current_currency = ''){
		if($current_currency == 'USD' || $current_currency == 'MXN' || $current_currency == 'EUR'){
			$this->session->set_userdata('current_currency', $current_currency);
		}
		if ($this->agent->is_referral()){
			redirect($this->agent->referrer());
		} else {
			redirect(base_url());
		}
	}
	
	function add_to_cart(){
		$redirect = '';
		if ($this->agent->is_referral()){
			if($this->input->post('add_product')){
				$redirect = $this->lb_functions->add_product($this->agent->referrer());
			}
		}
		redirect($redirect);
	}
	
	function delete_from_cart($rowid = 0){
		$redirect = '';
		if ($this->agent->is_referral()){
			$redirect = $this->lb_functions->delete_product($this->agent->referrer(), $rowid);
		}
		redirect($redirect);
	}
	
	function edit_qty_cart($qty = 0, $rowid = 0){
		$data = array(
			'rowid' => $rowid,
			'qty'   => $qty
		);
		$this->cart->update($data);
		if ($this->agent->is_referral()){
			redirect($this->agent->referrer());
		} else {
			redirect(base_url());
		}
	}
	
	function cancel_payment(){
		/*if($this->session->userdata('payment_data')){
			$this->session->unset_userdata('payment_data');
		}
		if($this->session->userdata('paypal_data')){
			$this->session->unset_userdata('paypal_data');
		}
		if($this->cart->total_items() > 0){
			$this->cart->destroy();
		}*/
		redirect('/carrito-de-compra');
	}
}
?>