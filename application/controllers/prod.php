<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prod extends CI_Controller {
	  function __construct(){
		  parent::__construct();
		}

		function index()
		{
			$send = array(
			'carousel_config' => $this->carousel->home(),
			'section_nav' => 'home'
		);
		load_view('home', $send);

		}
	}