<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Md_informacion extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
 
    function prod($id)
    {
        $query='SELECT p.id_product,p.product_name, p.product_code, b.brand, p.price, p.product_description, c.currency_name, p.before_price, p.url_images,p.url_techsheets ,s.sub_category,y.category,x.country_name,z.presentation, t.product_type FROM ops_product_types t, ops_products p, ops_currency c, ops_brands b, ops_sub_categories s, ops_countries x, ops_categories y, ops_presentation z  WHERE p.id_product = "'.$id.'"  AND p.id_brand=b.id_brand AND c.id_currency=p.id_currency AND s.id_sub_category=p.id_sub_category AND p.country_name_iso3=x.country_name_iso3 AND p.id_presentation=z.id_presentation AND p.id_product_type=t.id_product_type AND p.id_category=s.id_category';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    
    function getWish($user,$product)
    {
        $this->db->select('*');
        $this->db->from('ops_wish_list');
        $this->db->where('id_product',$product);
        $this->db->where('id_user',$user);
        $query = $this->db->get();
        $query = $query->num_rows();
        if($query) return false;
        else return true;
    }
}

?>