<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Md_insumos extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function productos($prod)
    {
        $query='SELECT p.id_product,p.product_name, p.product_code, b.brand, p.price, c.currency_name, p.before_price, p.url_images,p.id_sub_category,p.slug, s.sub_category, x.presentation FROM ops_products p, ops_currency c, ops_brands b, ops_sub_categories s, ops_categories y, ops_presentation x  WHERE p.id_brand=b.id_brand AND c.id_currency=p.id_currency AND y.id_category=p.id_category AND x.id_presentation=p.id_presentation AND s.id_sub_category=p.id_sub_category AND s.id_sub_category="'.$prod.'"';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function prod($prod,$inicio,$fin)
    {
        $query='SELECT p.id_product,p.product_name, p.product_code, b.brand, p.price, c.currency_name, p.before_price, p.url_images,p.id_sub_category,p.slug, s.sub_category, x.presentation FROM ops_products p, ops_currency c, ops_brands b, ops_sub_categories s, ops_categories y, ops_presentation x  WHERE p.id_brand=b.id_brand AND c.id_currency=p.id_currency AND y.id_category=p.id_category AND x.id_presentation=p.id_presentation AND s.id_sub_category=p.id_sub_category AND s.id_sub_category="'.$prod.'" LIMIT '.$inicio.','.$fin.'';
        $query=$this->db->query($query);
        return $query->result_array();
    }

    function info($id)
    {
        $query='SELECT * FROM ops_products  WHERE id_product="'.$id.'"';
        $query=$this->db->query($query);
        return $query->result();
    }


    function pais()
    {
        $query='SELECT c.country_name_iso3,c.country_name FROM ops_countries c, ops_categories s, ops_products p WHERE s.id_category=p.id_category AND  c.country_name_iso3=p.country_name_iso3 AND s.id_category="INS" ORDER BY c.country_name';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function marca()
    {
        $query='SELECT c.id_brand,c.brand FROM ops_brands c, ops_categories s, ops_products p WHERE s.id_category=p.id_category AND  c.id_brand=p.id_brand AND s.id_category="INS" ORDER BY c.brand';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function presentacion()
    {
        $query='SELECT c.id_presentation, c.presentation FROM ops_presentation c,  ops_categories s, ops_products p WHERE s.id_category=p.id_category AND  c.id_presentation=p.id_presentation AND s.id_category="INS" ORDER BY c.presentation';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function pais1($pais)
    {
        $query='SELECT country_name FROM ops_countries WHERE country_name_iso3="'.$pais.'"';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function marca1($marca)
    {
        $query='SELECT brand FROM ops_brands WHERE id_brand="'.$marca.'"';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function presentacion1($presen)
    {
        $query='SELECT presentation FROM ops_presentation  WHERE id_presentation="'.$presen.'"';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function catego1($prod)
    {
        $query='SELECT sub_category FROM ops_sub_categories WHERE id_sub_category="'.$prod.'"';
        $query=$this->db->query($query);
        return $query->result_array();
    }
    function catego()
    {
        $query='SELECT c.id_sub_category, c.sub_category FROM ops_sub_categories c , ops_categories s, ops_products p WHERE s.id_category=p.id_category AND  c.id_sub_category=p.id_sub_category AND s.id_category="INS" ORDER BY c.id_sub_category';
        $query=$this->db->query($query);
        return $query->result_array();
    }

    function filtro($marca,$presen,$pais,$prod)
    {
        $query='SELECT p.id_product,p.product_name, p.product_code, b.brand, p.price, c.currency_name, p.before_price, p.url_images,p.id_sub_category,p.slug, r.presentation, s.sub_category FROM ops_products p, ops_currency c, ops_brands b, ops_sub_categories s, ops_presentation r, ops_countries w  WHERE p.id_brand=b.id_brand AND c.id_currency=p.id_currency AND w.country_name_iso3=p.country_name_iso3 AND r.id_presentation=p.id_presentation AND s.id_sub_category=p.id_sub_category AND s.id_sub_category="'.$prod.'" AND r.id_presentation LIKE "'.$presen.'" AND b.id_brand LIKE "'.$marca.'" AND w.country_name_iso3 LIKE "'.$pais.'" ';
        $query=$this->db->query($query);
        return $query->result_array();
    }    
    function filtro2($marca,$presen,$pais,$prod,$inicio,$fin)
    {
        $query='SELECT p.id_product,p.product_name, p.product_code, b.brand, p.price, c.currency_name, p.before_price, p.url_images,p.id_sub_category,p.slug, r.presentation, s.sub_category FROM ops_products p, ops_currency c, ops_brands b, ops_sub_categories s, ops_presentation r, ops_countries w  WHERE p.id_brand=b.id_brand AND c.id_currency=p.id_currency AND w.country_name_iso3=p.country_name_iso3 AND r.id_presentation=p.id_presentation AND s.id_sub_category=p.id_sub_category AND s.id_sub_category="'.$prod.'" AND r.id_presentation LIKE "'.$presen.'" AND b.id_brand LIKE "'.$marca.'" AND w.country_name_iso3 LIKE "'.$pais.'" LIMIT '.$inicio.','.$fin.'';
        $query=$this->db->query($query);
        return $query->result_array();
    }    
    function insertWish($datos)
    {
        $this->db->insert('ops_wish_list',$datos);
    }
}