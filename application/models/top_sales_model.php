<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Top_sales_model extends CI_Model 
{
    
    function __construct() 
    {
        parent::__construct();        
        $this->load->database();
    }

    function getIdProduct()
    {
    	$this->db->select('id_product');
    	$this->db->from('ops_products');
    	$query = $this->db->get();
    	return $query->result_array();
    }

    function getSalesCount($product)
    {
    	$this->db->select('count(*)');
    	$this->db->from('ops_detail_order');
    	$this->db->where('id_product',$product);
    	$query = $this->db->get();
    	return $query->result_array();
    }
    function getDataPerCode($productCode)
    {
    	$query = "select producto.*,marca.*,moneda.*,presentacion.* FROM ops_presentation presentacion, ops_currency moneda, ops_brands marca, ops_products producto where producto.id_product = $productCode and marca.id_brand = producto.id_brand AND producto.id_currency = moneda.id_currency AND presentacion.id_presentation = producto.id_presentation";
    	$query = $this->db->query($query);    	
    	return $query->result_array();
    }
    function fillCacheSales($products)
    {
        $this->db->trans_begin();
        $this->db->insert_batch('ops_cache_top_sales',$products);
        if($this->db->trans_status() == FALSE) $this->db->trans_rollback();
        else $this->db->trans_commit();
    }
}
?>