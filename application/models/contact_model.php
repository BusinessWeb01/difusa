<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }
    
    public function save($data){
        $this->db->trans_begin();
        
	$this->db->insert('ops_contacts',$data);
	
        $id = $this->db->insert_id();
	
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
	    return false;
	}
	else{
            $this->db->trans_commit();
	    return $id;
	}
    }
    
    public function save_own_ring($data){
        $this->db->trans_begin();
        
	$this->db->insert('contacts_own_ring',$data);
	
        $id = $this->db->insert_id();
	
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
	    return false;
	}
	else{
            $this->db->trans_commit();
	    return $id;
	}
    }
}