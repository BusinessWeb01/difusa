<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Registra_model extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->database();
    }
 
    function registro($data)
    {
        $this->db->trans_begin();

        $this->db->insert('ops_clients',$data);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }    
    function registroFactura($fact)
    {
        $this->db->trans_begin();

        $this->db->select('max(id_client) as max');
        $this->db->from('ops_clients');
        $query = $this->db->get();
        $id = $query->result_array();
        $fact['id_client'] = $id[0]['max'];
        $this->db->insert('ops_client_address_bill',$fact);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }
    function getMails()
    {
        $this->db->select('contact_mail as mails');
        $this->db->from('ops_clients');
        $query = $this->db->get();
        return $query->result_array();
    }
    function newUserSuscription()
    {
        $this->db->select('max(id_client) as max');
        $this->db->from('ops_clients');
        $query = $this->db->get();
        $id= $query->result_array();
        $idUser = $id[0]['max'];
        return $idUser;
    }
    function insertNewSuscription($suscriptions)
    {
        $this->db->trans_begin();

        $this->db->insert_batch('ops_subscriptions_ops_userss',$suscriptions);

        if($this->db->trans_status() === FALSE)
            $this->db->trans_rollback();
        else
            $this->db->trans_commit();
    }
}