<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Admin_model extends CI_Model 
{
    function __construct() 
    {
        parent::__construct();
        $this->load->database();
    }

	function getSubCategory()
	{
		$this->db->select('id_sub_category,sub_category');
		$this->db->from('sub_categories');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getCountry()
	{
		$this->db->select('country_name_iso3,country_name');
		$this->db->from('countries');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getProductType()
	{
		$this->db->select('id_product_type,product_type');
		$this->db->from('product_types');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getBrands()
	{
		$this->db->select('id_brand,brand');
		$this->db->from('brands');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getPresentation()
	{
		$this->db->select('id_presentation,presentation');
		$this->db->from('presentation');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getUsersName()
	{
		$this->db->select('id_client,client_name,last_name_client');
		$this->db->from('clients');
		$query = $this->db->get();
		return $query->result_array();
	}

	function topSales()
	{
		
	}
}
?>