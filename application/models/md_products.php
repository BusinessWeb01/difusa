<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_Products extends CI_Model{

	public function __construct(){
		$this->load->database();
	}
	/*----------------------- klamore ------------------------------------------------------------------*/
	public function save_product($data = false){
		if($data){

                        //ndp 20150718 - IK
                        log_message('debug', 'md_products.save_product().$data= '.print_r($data,TRUE));
                        
			$this->db->trans_begin();
			
			date_default_timezone_set('Mexico/General');
			$date = date("Y-m-d H:i:s");
			
			$data['data_product']['created_at'] = $date;
			$data['data_product']['updated_at'] = $date;
			
			if($data['data_product']['ring']){
				$data['data_product']['ring'] = 1;
			}
			else{
				$data['data_product']['ring'] = 0;
			}
			$this->db->insert('products', $data['data_product']);
			$product_id = $this->db->insert_id();
			
			
			if($data['categories_rings']){
				$inser_rel_category_rings_product = array(
					'id_category_rings' => $data['categories_rings'],
					'id_product' => $product_id,
					'created_at' => $date
				);
				$this->db->insert('rel_category_rings_product', $inser_rel_category_rings_product);
			}
			
			if($data['category_collection']){
				$inser_rel_category_collection_product = array(
					'id_category_collection' => $data['category_collection'],
					'id_product' => $product_id,
					'created_at' => $date
				);
				$this->db->insert('rel_category_collection_product', $inser_rel_category_collection_product);
			}
			
			if($data['img_product']){
				foreach($data['img_product'] as $key => $value){
					$inser_img = array(
						'id_product' => $product_id,
						'meta_key' => 'img',
						'meta_value' => $value['file_name'],
						'created_at' => $date,
						'updated_at' => $date
					);
					$this->db->insert('products_meta', $inser_img);
				}
			}
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}
			else{
				$this->db->trans_commit();
				return true;
			}
		}
		else{
			return false;
		}
	}
	
	function get_img_meta_for_product($product_id = 0){
		$this->db->select('
			id_product_meta,
			id_product,
			meta_value
		');
		$this->db->where('id_product', $product_id);
		$this->db->from('products_meta');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
	}
	
	function get_rel_category_rings_product($product_id = 0){
		$this->db->select('
			id_rel_category_rings_product,
			id_category_rings,
			id_product
		');
		$this->db->where('id_product', $product_id);
		$this->db->from('rel_category_rings_product');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
	}
	
	function get_rel_category_collection_product($product_id = 0){
		$this->db->select('
			id_rel_category_collection_product,
			id_category_collection,
			id_product
		');
		$this->db->where('id_product', $product_id);
		$this->db->from('rel_category_collection_product');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
	}
	
	function get_all_product_info_by_id($id = 0){
		$this->db->select('
			id_product,
			ring,
			promotion,
			name,
			model,
			slug,
			description,
			price,
			id_rings_color,
			id_rings_kilate,
			id_rings_point,
                        default_points
		'); //ndp 20150718 - default_points agregado


		$this->db->where('id_product', $id);
		$this->db->from('products');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$row->img = $this->get_img_meta_for_product($row->id_product);
				$row->category_rings_product = $this->get_rel_category_rings_product($row->id_product);
				$row->category_collection_product = $this->get_rel_category_collection_product($row->id_product);
				$d[] =  $row;
			}
		}
		return $d;
	}
	
	public function update_product($data = false, $product_id = 0){
		if($data){
			$this->db->trans_begin();
			
			date_default_timezone_set('Mexico/General');
			$date = date("Y-m-d H:i:s");
			
			$data['data_product']['updated_at'] = $date;
			
			$this->db->where('id_product', $product_id);
			$this->db->update('products', $data['data_product']);
			
			
			$this->db->where('id_product', $product_id);
			$this->db->delete('rel_category_rings_product');
			
			if($data['categories_rings']){
				$insert = array(
					'id_category_rings' => $data['categories_rings'],
					'id_product' => $product_id,
					'created_at' => $date
				);
				$this->db->insert('rel_category_rings_product', $insert);
			}
			
			$this->db->where('id_product', $product_id);
			$this->db->delete('rel_category_collection_product');
			
			if($data['category_collection']){
				$insert = array(
					'id_category_collection' => $data['category_collection'],
					'id_product' => $product_id,
					'created_at' => $date
				);
				$this->db->insert('rel_category_collection_product', $insert);
			}
			
		
			if($data['img_product']){
				foreach($data['img_product'] as $key => $value){
					$inser_img = array(
						'id_product' => $product_id,
						'meta_key' => 'img',
						'meta_value' => $value['file_name'],
						'created_at' => $date,
						'updated_at' => $date
					);
					$this->db->insert('products_meta', $inser_img);
				}
			}
			
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}
			else{
				$this->db->trans_commit();
				return true;
			}
		}
		else{
			return false;
		}
	}
	
	/*----------------------- EOL: klamore ------------------------------------------------------------------*/
	
	function get_categories(){
		$this->db->select('
			id,
			id `cat_id`,
			name,
			slug,
			description
		');
		$this->db->order_by('name', 'ASC');
		$this->db->from('categories');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
	}
	function get_manufacturers(){
		$this->db->select('
			id,
			id `manufacturer_id`,
			name,
			slug,
			description
		');
		$this->db->order_by('name', 'ASC');
		$this->db->from('manufacturers');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
	}
	
	function get_product_by_id( $id = 0 ){
		$this->db->select('
			id,
			id `product_id`,
			name,
			slug,
			description,
			price,
			currency,
			model,
			height,
			width,
			long,
			weight,
			promotion
		');
		$this->db->where('id', $id);
		$this->db->from('products');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d['id'] =  $row->product_id;
				$d['name'] =  $row->name;
				$d['slug'] =  $row->slug;
				$d['description'] =  $row->description;
				$d['price'] =  $row->price;
				$d['currency'] =  $row->currency;
				$d['model'] =  $row->model;
				$d['height'] =  $row->height;
				$d['width'] =  $row->width;
				$d['long'] =  $row->long;
				$d['weight'] =  $row->weight;
				$d['promotion'] =  $row->promotion;
			}
		}
		return $d;
	}
	
	function get_manufacturers_product( $id = 0 ){
		$this->db->select('
			id,
			id `rel_manu_prod_id`,
			manufacturer_id,
			product_id
		');
		$this->db->where('product_id', $id);
		$this->db->from('rel_manufacturer_product');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[$row->manufacturer_id] =  $row->manufacturer_id;
			}
		}
		return $d;
	}
	
	function get_categories_product( $id = 0 ){
		$this->db->select('
			id,
			id `rel_cate_prod_id`,
			category_id,
			product_id
		');
		$this->db->where('product_id', $id);
		$this->db->from('rel_category_product');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[$row->category_id] =  $row->category_id;
			}
		}
		return $d;
	}
	
	function user_is_admin($user_id = 0){
		if($user_id == 0){
			return false;
		}
		else{
			//
			$this->db->select('
				id,
				username,
				activated,
				id_profiles
			');
			$this->db->from('users');
			$this->db->where('id', $user_id);
			$d = false;
			$q = $this->db->get();
			if ($q->num_rows > 0){
				foreach ($q->result() as $row){
					$d['profile'] =  $row->id_profiles;
				}
				if($d['profile'] == 1){
					return true;
				}
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
	}
}