<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login_model extends CI_Model {
 
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
 
    function logueo($username,$password)
    {             
        $this->db->select('*');
        $this->db->from('ops_clients');
        $this->db->where('user_name',$username);
        $this->db->where('password',$password);

        $query = $this->db->get();
        if($query -> num_rows() == 1) return $query->result_array();
        else return false;
    }
    function admin($username,$password)
    {
        $this->db->select('*');
        $this->db->from('ops_users');
        $this->db->where('username',$username);
        $this->db->where('password',$password);
        $query = $this->db->get();
        if($query -> num_rows() == 1) return $query->result_array();
        else return false;
    }
}
?>