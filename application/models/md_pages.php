<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Md_pages extends CI_Model{
    
    public function __construct(){
        $this->load->database();
    }
    
    function get_web_section_by_name($name = ''){
		$this->db->select('
			id_web_section,
			name
		');
		$this->db->where('name', $name);
		$this->db->from('web_sections');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[$row->id_web_section] =  $row->name;
			}
		}
		return $d;
    }
    
    function get_banners_for_section($section_name = 'Home'){
		$section = $this->get_web_section_by_name($section_name);
		$id_section = 0;
		if($section){
			$id_section = key($section);
		}
		$this->db->select('
			id_banner,
			id_web_section,
			url,
			image
		');
		$this->db->where('id_web_section', $id_section);
		$this->db->from('banners');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
    }	

	function get_web_sections(){
		$this->db->select('
			id_web_section,
			name
		');
		$this->db->order_by('id_web_section', 'ASC');
		$this->db->from('web_sections');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
			$d[$row->id_web_section] =  $row->name;
			}
		}
		return $d;
    }
	
	public function get_page_by($field = '', $value = ''){
        $this->db->select('
            id_page,
			id_page `page_id`,
			title,
			slug,
			content,
			author
			');
		$this->db->where($field, $value);
		$this->db->from('pages');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
				foreach ($q->result() as $row){
					$d['page_id'] =  $row->id_page;
					$d['title'] =  $row->title;
					$d['slug'] =  $row->slug;
					$d['content'] =  $row->content;
					$d['author'] =  $row->author;
			}
		}
		return $d;
    }
	
	function get_number_of_products_searched($product_type,$category,$sub_category){
		$this->db->select('
			products.id_product
		');		
		$this->db->from('products');
		if($product_type){
			if($product_type->slug == 'promociones'){
				$this->db->where('promotion', 1);
			}else if($product_type->slug == 'nuevos-productos'){
				$this->db->order_by('products.created_at', 'DESC');
				$this->db->limit(8);
			}else if($product_type->slug == 'mas-vendidos'){
				$this->db->order_by('products.amount_sold', 'DESC');
				$this->db->limit(8);
			}else{
				$this->db->where('id_product_type', $product_type->id_product_type);
			}
		}
		if($category){
			$this->db->where('id_category', $category->id_category);
		}
		if($sub_category){
			$this->db->where('id_sub_category', $sub_category->id_sub_category);
		}
		$q = $this->db->get();
		return $q->num_rows;
    }
	
	public function get_product_type_by($field = '', $value = ''){
		$this->db->select('
            id_product_type,
			product_type,
			slug
		');
		$this->db->where($field, $value);
		$this->db->from('product_types');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
				foreach ($q->result() as $row){
					$d =  $row;
			}
		}
		return $d;
	}
	
	public function get_category_by($field = '', $value = ''){
		$this->db->select('
            id_category,
			category,
			slug
		');
		$this->db->where($field, $value);
		$this->db->from('categories');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
				foreach ($q->result() as $row){
					$d =  $row;
			}
		}
		return $d;
	}
	
	public function get_sub_category_by($field = '', $value = ''){
		$this->db->select('
            id_sub_category,
			sub_category,
			slug
		');
		$this->db->where($field, $value);
		$this->db->from('sub_categories');
		$d = false;
		$q = $this->db->get();
		if ($q->num_rows > 0){
				foreach ($q->result() as $row){
					$d =  $row;
			}
		}
		return $d;
	}
	
	public function get_products($product_type,$category,$sub_category,$begin_in = 0){
		$this->db->select('
			products.id_product,
			products.product_name,
			products.product_code,
			products.price,
			products.slug
		');
		
		$this->db->from('products');
		if($product_type){
			if($product_type->slug == 'nuevos-productos'){
				$this->db->order_by('products.created_at', 'DESC');
				$this->db->limit(8);
			}else if($product_type->slug == 'mas-vendidos'){
				$this->db->order_by('products.amount_sold', 'DESC');
				$this->db->limit(8);
			}else{
				if($product_type->slug == 'promociones'){
					$this->db->where('promotion', 1);
				}else{
					$this->db->where('id_product_type', $product_type->id_product_type);
				}
				$this->db->order_by('products.product_name, products.product_code', 'ASC');
				$this->db->limit(8, $begin_in);
			}			
		}
		if($category){
			$this->db->where('id_category', $category->id_category);
		}
		if($sub_category){
			$this->db->where('id_sub_category', $sub_category->id_sub_category);
		}
		
		$d = false;
		$q = $this->db->get();
		
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				/*$row->images = $this->get_images_by_table_and_id($row->id_product);
				$row->path_img = get_option('images_dir').'products/';
				$row->link = base_url().'anillos-de-compromiso/'.$row->category_slug.'/'.$row->product_slug;*/
				$row->images = $this->get_images_by_table_and_id($row->id_product);
				$row->path_img = get_option('images_dir').'/';
				$row->link = base_url().'product-detail/'.$row->product_code.'/'.$row->slug;
				$d[] =  $row;
			}
		}
		return $d;
	}
	
	function get_images_by_table_and_id($id = 0, $table = 'products_meta', $name_id = 'id_product'){
		
		$this->db->select('
			meta_value
		');
		$this->db->where($name_id, $id);
		$this->db->where('meta_key', 'img');
		$this->db->from($table);
		$d = array('default.png');
		$q = $this->db->get();
		
		if ($q->num_rows>0) {
			$d = array();
			foreach ($q->result() as $row) {
			$d[] =  $row->meta_value;
			}
		}
		return $d;
    }
	
	function get_product_detail($product_code,$slug){
		$this->db->select('
			products.id_product,
			products.product_name,
			products.product_code,
			products.price,
			products.slug,
			products.product_description,
			brands.brand,
			countries_region.country_region,
			units_measure.unit_measure,
			categories.category,
			sub_categories.sub_category,
			product_types.product_type
		');
		$this->db->from('products');
		$this->db->join('brands', 'products.id_brand = brands.id_brand','left');
		$this->db->join('countries_region', 'products.id_country_region = countries_region.id_country_region','left');
		$this->db->join('units_measure', 'products.id_unit_measure = units_measure.id_unit_measure','left');
		$this->db->join('categories', 'products.id_category = categories.id_category','left');
		$this->db->join('sub_categories', 'products.id_sub_category = sub_categories.id_sub_category','left');
		$this->db->join('product_types', 'products.id_product_type = product_types.id_product_type','left');
		
		$this->db->where('products.product_code', $product_code);
		$this->db->where('products.slug', $slug);
		$d = false;
		$q = $this->db->get();
		
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
	}
	
	function get_product_by($field = '', $value = ''){
		$this->db->select('
			products.id_product,
			products.product_name,
			products.product_code,
			products.price,
			products.slug,
			products.product_description,
			brands.brand,
			countries_region.country_region,
			units_measure.unit_measure,
			categories.category,
			sub_categories.sub_category,
			product_types.product_type
		');
		$this->db->from('products');
		$this->db->join('brands', 'products.id_brand = brands.id_brand','left');
		$this->db->join('countries_region', 'products.id_country_region = countries_region.id_country_region','left');
		$this->db->join('units_measure', 'products.id_unit_measure = units_measure.id_unit_measure','left');
		$this->db->join('categories', 'products.id_category = categories.id_category','left');
		$this->db->join('sub_categories', 'products.id_sub_category = sub_categories.id_sub_category','left');
		$this->db->join('product_types', 'products.id_product_type = product_types.id_product_type','left');
		
		$this->db->where('products.product_code', $value);
		$d = false;
		$q = $this->db->get();
		
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d[] =  $row;
			}
		}
		return $d;
    }
	
	function get_option($option = '', $default = false, $single = true){
		$this->load->database();
		$this->db->select('
			id_option,
			id_option `option_id`,
			option_name,
			option_name `name`,
			option_value,
			option_value `value`
		');
		$this->db->where('option_name', $option);
		$this->db->from('options');
		$d = $default;
		$q = $this->db->get();
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
			if($single){
				$d = $row->option_value;
				break;
			} else {
				$d[] =  $row;
			}
			}
		}
		return $d;
    }
	
	function get_info_of_payment_by_folio($folio = 0){
	
	log_message('debug', '...get_info_of_payment_by_folio($folio = 0) =='.$folio);
	

		$this->db->select('
            id_pago,
			pagado,
			folio,
			metodo_pago,
			fecha_registro,
			fecha_confirmacion,
			informacion,
			respuesta_metodo_pago
		');
		$this->db->where('folio', $folio);
		$this->db->where('pagado', 0);
		$this->db->from('pagos');
		$d = false;
		$q = $this->db->get();
		
		log_message('debug', '///////////////////////////////////////////////////////////////////////////////////');	
		log_message('debug', 'md_pages.get_info_of_payment_by_folio.$this->db:'.print_r($this->db, TRUE));				
		log_message('debug', 'md_pages.get_info_of_payment_by_folio.$q:'.print_r($q, TRUE));
		log_message('debug', '///////////////////////////////////////////////////////////////////////////////////');	
		
		if ($q->num_rows > 0){
			foreach ($q->result() as $row){
				$d =  $row;
			}
		}
		return $d;
    }
}