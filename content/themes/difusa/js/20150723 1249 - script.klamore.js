/*----------------------------------------------------------------------------------------------*/
function get_price(price, qty) {
    var real_price = price * qty;
    real_price.toFixed(2);
    var string = numeral(real_price).format('$0,0.00');
    return string;
}
function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function IsPhone(phone) {
  var regex = /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$/;
  return regex.test(phone);
}

//20150716 ndp - modificacion para validacion de rfc
function IsValidRFC(rfc) {

  var regex = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/;

  return regex.test(rfc);
}
/*----------------------------------------------------------------------------------------------*/
$(window).load(function(){
    var top = $('#load_in_section_cate').length;
    var top_title = $('#load_in_title_section').length;
    if (top == 1) {
        $('html, body').animate({
            scrollTop : $("#load_in_section_cate").offset().top
        },0);
    }
    
    if (top_title == 1) {
        $('html, body').animate({
            scrollTop : $("#load_in_title_section").offset().top
        },0);
    }
    
    var load_in_title_ring = $('#load_in_title_ring').length;
    if (load_in_title_ring == 1) {
        $('html, body').animate({
            scrollTop : $("#load_in_title_ring").offset().top
        },0);
    }
    
    var load_in_page = $('#load_in_here').length;
    if (load_in_page == 1) {
        $('html, body').animate({
            scrollTop : $("#load_in_here").offset().top
        },0);
    }
    
});
/*----------------------------------------------------------------------------------------------*/
$(document).ready(function(){
    
    $('.size-img-cat-rings').width(200);
    $('.size-img-cat-rings').height(200);
    
    $('.size-img-cat-rings-hover').width(80);
    $('.size-img-cat-rings-hover').height(80);
    
    $('.size-img-cat-rings-hover-md').width(120);
    $('.size-img-cat-rings-hover-md').height(120);
    
    $('.size-img-product-cate').width(140);
    $('.size-img-product-cate').height(140);
    
    $('.size-product-principal').width(400);
    //$('.size-product-principal').height(400);
    $('.size-img-more-products').width(70);
    $('.size-img-more-products').height(70);
    $('.caro-images-ring').carousel({
        interval: false
    });
    
    $('.size-img-pro-add-cart').width(90);
    $('.size-img-pro-add-cart').height(90);

		/*NDP 20150706*/
//    $('.size-img-normal-aboutus').height(250);
    $('.size-img-normal-aboutus').height(291);
    
    $("#btn_add_product_in_cart").click(function(){
	$("#modal_add_cart").modal({
	    backdrop: 'static'
	});
        $( "#btn_go_to_list" ).click(function() {
	    $('#next_option').val('list');
	    document.forms["form_add_product"].submit();
	});
	$( "#btn_go_to_category" ).click(function() {
	    $('#next_option').val('return');
	    document.forms["form_add_product"].submit();
	});
    });
    
    
    $(".btn_img_searc").click(function(){
	$( "#form_search" ).submit();
    });
    
    /*
        http://bweb.mx/klamore/
    */
    $("#link_home_anillos").hover(function() {
        $("#img_home_anillos").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/compromiso.png");
        $("#img_home_enamora").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/enamora_.png");
    },function() {
        $("#img_home_anillos").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/compromiso-claro.png");
        $("#img_home_enamora").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/enamora-color.png");
    });
    
    $("#link_home_colecciones").hover(function() {
        $("#img_home_sorprende").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/sorprende.png");
        $("#img_home_coleciones").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/colecciones.png");
    },function() {
        $("#img_home_sorprende").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/sorprende-color.png");
        $("#img_home_coleciones").attr("src","http://bweb.mx/klamore/content/themes/klamore/img/colecciones-claro.png");
    });
    
    $('.carousel-product').carousel({
        interval: 1000,
        pause: false
    });
    
    $('#carousel_related_products').carousel({
        interval: false
    });
    
    $(".container-product-in-cat", this).hover(function() {
        $( ".carousel-product", this ).show();
        $( ".img-default-produ", this ).hide();
    },function() {
        $( ".carousel-product", this ).hide();
        $( ".img-default-produ", this ).show();
    });
    
    
    /*$('#quantity').keydown(function() {
        return false;
    });*/
    
    
    $('#quantity').change( function() {
        text = $(this).val();
        var qty = 0;
        if (text) {
            qty = parseInt(text);
        }
        var product_price = $( "#product_price").val();
        product_price = parseFloat(product_price);
        var the_price = get_price(product_price, qty);
        $( "#the_price_now" ).text(the_price);
        $( "#the_price_now_modal" ).text(the_price);
    });

    
    $('#more_quantity').click(function(){
        var qty = $("#quantity").val();
        if (qty) {
            qty = parseInt(qty);
            if (qty >= 999) {
                qty = 998;
            }
        }
        else{
            qty = 0;
        }
        var product_price = $( "#product_price").val();
        product_price = parseFloat(product_price);    
        var new_qty = qty + 1;
        var the_price = get_price(product_price, new_qty);
        $( "#quantity").val(new_qty);
        $( "#the_price_now" ).text(the_price);
        $( "#the_price_now_modal" ).text(the_price);
        return false;
    });
    
    $("#quantity").mask("9?99",{placeholder:" "});

    




    
    $( "#point" ).change(function() {
        var point = $(this).find("option:selected").attr("id");
        point = point.replace("point_", "");
        point = parseInt(point);
        var kilate = $("#kilate").find("option:selected").attr("id");
        kilate = kilate.replace("kilate_", "");
        kilate = kilate.replace("k", "");
        kilate = kilate.replace("K", "");
        kilate = parseInt(kilate);
        var price_ring = get_price_with_universal_prices(kilate, point);
        var qty = $( "#quantity").val();
        if (qty) {
            qty = parseInt(qty);
            if (qty > 999) {
                qty = 999;
            }
        }
        else{
            qty = 0;
        }
        var the_price = get_price(price_ring, qty);
        $( "#product_price").val(price_ring);
        $( "#quantity").val(qty);
        $( "#the_price_now" ).text(the_price);
        $( "#the_price_now_modal" ).text(the_price);
    });
    
    $( "#kilate" ).change(function() {
        
        var kilate = $(this).find("option:selected").attr("id");
        kilate = kilate.replace("kilate_", "");
        kilate = kilate.replace("k", "");
        kilate = kilate.replace("K", "");
        kilate = parseInt(kilate);
        
        var point = $("#point").find("option:selected").attr("id");
        point = point.replace("point_", "");
        point = parseInt(point);
        
        var price_ring = get_price_with_universal_prices(kilate, point);
        var qty = $( "#quantity").val();
        if (qty) {
            qty = parseInt(qty);
            if (qty > 999) {
                qty = 999;
            }
        }
        else{
            qty = 0;
        }
        var the_price = get_price(price_ring, qty);
        $( "#product_price").val(price_ring);
        $( "#the_price_now" ).text(the_price);
        $( "#the_price_now_modal" ).text(the_price);
    });
    
    $('.border-more-products').click(function(){
        var image = $(this).find("img").attr("id");
        var path_img = $( "#path_images").val();
        var new_image = path_img.concat(image);
        $("#the_img_ring_now").data('zoom-image', new_image).elevateZoom({
            zoomWindowPosition: "demo-container",
            zoomWindowHeight: $( "#demo-container" ).height()+10,
            zoomWindowWidth: $( "#demo-container" ).width()+20,
            borderSize: 0,
            easing:true
        });
        $("#the_img_ring_now").attr("src",new_image);
        return false;
    });
    $('#the_img_ring_now').elevateZoom({
        zoomWindowPosition: "demo-container",
        zoomWindowHeight: $( "#demo-container" ).height()+10,
        zoomWindowWidth: $( "#demo-container" ).width()+20,
        borderSize: 0,
        easing:true
    }); 
    
    /* I-control de la manipulación dinámica del carrito de compra */
    $('#btn_ordenar').click(function(){
        $( "#btn_ordenar" ).hide();
        $( ".step-1-cart" ).show();
        $('html, body').animate({
            scrollTop : $("#step_1_cart").offset().top
        },0);
        return false;
    });
    
    $('#btn_si_factura').click(function(){
        $( ".step-1-cart" ).hide();
        $( ".step-2-cart" ).show();
        $('html, body').animate({
            scrollTop : $("#step_2_cart").offset().top
        },0);
    });
    $('#btn_no_factura').click(function(){
        $( ".step-1-cart" ).hide();
        $( ".step-2-cart" ).hide();
        $( ".step-3-cart" ).show();
        $('html, body').animate({
            scrollTop : $("#step_3_cart").offset().top
        },0);
    });
    
    
    $('#btn_next_step').click(function(){
        var fields = [
            "#nombre_cliente",
            "#apellido_paterno_cliente",
            "#apellido_materno_cliente",
            "#telefono_cliente",
            "#celular_cliente",
            "#email_cliente"
        ];
        var num_fields = fields.length;
        var all_fields = true;
        for(var i = 0; i < num_fields; i++){
            if (!$(fields[i]).val()) {
                all_fields = false;
            }
        }
        if (all_fields) {
            if (IsEmail($("#email_cliente").val())) {
                if (IsPhone($("#telefono_cliente").val()) && IsPhone($("#celular_cliente").val())) {
                    $("#modal_opcion_factura").modal();
                }
                else{
                    $("#modal_phone_error").modal();
                }
            }
            else{
                $("#modal_email_error").modal();
            }
        }
        else{
            $("#modal_missing_fields").modal();
        }
        return false;
    });
    
    
    $('#btn_next_step_2').click(

        function(){
            var fields = [
                "#razon_social_factura",
                "#calle_factura",
                "#num_exterior_factura",
                "#colonia_factura",
                "#localidad_factura",
                "#delegacion_factura",
                "#estado_factura",
                "#cp_factura",
                "#email_factura",
                "#rfc_factura"
            ];
            var num_fields = fields.length;
            var all_fields = true;
            for(var i = 0; i < num_fields; i++){
                if (!$(fields[i]).val()) {
                    all_fields = false;
                }
            }
            //ndp 20150716
            if (all_fields) {
                //ndp 21050716 - validacion rfc
                //$("#rfc_factura").mask("************?*",{placeholder:" "});
                //alert("RFC="+$("#rfc_factura").val());
                ///alert("IS VALID RFC="+IsValidRFC($("#rfc_factura").val()));
                if(!IsValidRFC($("#rfc_factura").val())){
                    $("#modal_rfc_error ").modal();
                    
                    $( ".step-2-cart" ).show();
                    $('html, body').animate({scrollTop : $("#step_2_cart").offset().top},0);
                    return;
                }

                if (IsEmail($("#email_factura").val())) {
                    $( ".step-2-cart" ).hide();
                    $( ".step-3-cart" ).show();
                    $('html, body').animate({
                        scrollTop : $("#step_3_cart").offset().top
                    },0);
                }
                else{
                    $("#modal_email_error").modal();
                }
            }
            else{
                $("#modal_missing_fields").modal();
            }
            return false;
        }
    );
    
    $('#btn_next_step_3').click(function(){
        var fields = [
            "#nombre_envio",
            "#calle_envio",
            "#num_exterior_envio",
            "#colonia_envio",
            "#localidad_envio",
            "#delegacion_envio",
            "#estado_envio",
            "#cp_envio",
            "#telefono_1_envio"
        ];
        var num_fields = fields.length;
        var all_fields = true;
        for(var i = 0; i < num_fields; i++){
            if (!$(fields[i]).val()) {
                all_fields = false;
            }
        }
        if (all_fields) {
            if (IsPhone($("#telefono_1_envio").val())) {
                $( ".step-3-cart" ).hide();
                $( ".step-4-cart" ).show();
                $('html, body').animate({
                    scrollTop : $("#step_4_cart").offset().top
                },0);
            }
            else{
                $("#modal_phone_error").modal();
            }
        }
        else{
            $("#modal_missing_fields").modal();
        }
        return false;
    });
    
    $('#btn_comprar').click(function(){
        var send = true;
        if (!$('.payment_option').is(':checked')){
            send = false;
            $("#modal_missing_payment_option").modal();
        }
        if (!$('#terms').is(':checked')){
            send = false;
            $("#modal_missing_terms").modal();
        }
        if (send) {
            $( "#form_facturacion" ).submit();
        }
        return false;
    });
    /* F-control de la manipulación dinámica del carrito de compra */
    
    /* I- resumen de la compra */
    $('#btn_cancelar_compra').click(function(){
        /*$("#modal_cancel_payment").modal({
            backdrop: 'static'
        });
        $('#yes_cancel_payment').click(function(){
            $( "#form_cancel_payment" ).submit();
        });*/
        //parent.history.back();
        $( "#form_cancel_payment" ).submit();
        return false;
    });
    
    $('#btn_cancelar_compra').click(function(){
        return false;
    });
    
    /* F- de la compra */
});
window.onload = document.getElementById('column').style.display = 'none';
function muestraFormulario2(ids)
{
    if(!ids.checked )
    {
        document.getElementById('column').checked = 'none'; 
    }
    else
    {
        document.getElementById('column').checked = 'block';        
    }
}
/*----------------------------------------------------------------------------------------------*/